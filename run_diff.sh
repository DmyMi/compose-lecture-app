#!/bin/bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 PROJECT1 PROJECT2"
    exit 1
fi

PROJECT1="$1"
PROJECT2="$2"

BASE_DIR="lectures"
SUB_PATH="src/{FOLDER}/kotlin/cloud/dmytrominochkin/composelecture"

FOLDERS_TO_COMPARE=("commonMain" "androidMain" "desktopMain")

for folder in "${FOLDERS_TO_COMPARE[@]}"; do
    FOLDER_1="$BASE_DIR/$PROJECT1/$SUB_PATH/$PROJECT1"
    FOLDER_2="$BASE_DIR/$PROJECT2/$SUB_PATH/$PROJECT2"

    # Replace placeholder with the actual folder name
    FOLDER_1=${FOLDER_1/\{FOLDER\}/$folder}
    FOLDER_2=${FOLDER_2/\{FOLDER\}/$folder}

    # Check if both directories exist
    if [ -d "$FOLDER_1" ] || [ -d "$FOLDER_2" ]; then
        echo "Comparing $FOLDER_1 and $FOLDER_2:"
        diff -BburNst --ignore-blank-lines "$FOLDER_1" "$FOLDER_2"
        echo "--------------------------------------"
    else
        echo "One or both directories for $folder do not exist:"
        echo "$FOLDER_1"
        echo "$FOLDER_2"
        echo "--------------------------------------"
    fi
done
