package cloud.dmytrominochkin.composelectures.injection

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.injection.datasource.MockNetDataSource
import cloud.dmytrominochkin.composelectures.injection.ui.profile.Content
import cloud.dmytrominochkin.composelectures.injection.ui.profile.ProfileState
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun ProfilePreview() {
    ComposeLectureTheme {
        Surface {
            Content(state = ProfileState(user = MockNetDataSource.users[0]), Modifier.fillMaxSize())
        }
    }
}
