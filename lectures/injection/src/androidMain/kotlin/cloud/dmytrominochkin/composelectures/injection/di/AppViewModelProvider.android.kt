package cloud.dmytrominochkin.composelectures.injection.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.CreationExtras

/**
 * Extension function that queries for [android.app.Application] object and returns an instance of
 * [AppContainerProvider].
 */
internal actual fun CreationExtras.socialApplication(): AppContainerProvider =
    (this[ViewModelProvider.AndroidViewModelFactory.APPLICATION_KEY] as AppContainerProvider)