package cloud.dmytrominochkin.composelectures.injection

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.injection.datasource.MockNetDataSource
import cloud.dmytrominochkin.composelectures.injection.ui.feed.Content
import cloud.dmytrominochkin.composelectures.injection.ui.feed.FeedState
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun FeedPreview() {
    ComposeLectureTheme {
        Content(SnackbarHostState(), FeedState(users = MockNetDataSource.users), {}, {}, {}, {}, {})
    }
}
