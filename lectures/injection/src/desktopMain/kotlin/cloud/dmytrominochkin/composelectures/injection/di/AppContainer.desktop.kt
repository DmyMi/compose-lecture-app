package cloud.dmytrominochkin.composelectures.injection.di

internal object DesktopApp : AppContainerProvider {
    override val appContainer: AppContainer by lazy {
        AppDataContainer()
    }

}