package cloud.dmytrominochkin.composelectures.injection.di

import androidx.lifecycle.viewmodel.CreationExtras

/**
 * Extension function that queries for some object and returns an instance of
 * [AppContainerProvider].
 */
internal actual fun CreationExtras.socialApplication(): AppContainerProvider = DesktopApp
