package cloud.dmytrominochkin.composelectures.injection.repository

import cloud.dmytrominochkin.composelectures.injection.model.User

interface UserRepository {
    suspend fun getUser(id: Int): Result<User>
    suspend fun loadFeed(): Result<List<User>>
}
