package cloud.dmytrominochkin.composelectures.injection.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cloud.dmytrominochkin.composelectures.injection.model.User
import cloud.dmytrominochkin.composelectures.injection.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

// For a simple application like this it's an overkill to have 2 viewmodels.
// One is more then enough to handle this flow. Just for demo purposes
internal class ProfileViewModel(
    private val usersRepository: UserRepository
) : ViewModel() {
    private val _uiState = MutableStateFlow(ProfileState())
    val uiState = _uiState.asStateFlow()

    fun getUser(id: Int) {
        _uiState.update { it.copy(isLoading = true) }
        viewModelScope.launch {
            usersRepository.getUser(id)
                .onSuccess {
                    _uiState.update { oldState -> oldState.copy(isLoading = false, user = it) }
                }
        }
    }
}

internal data class ProfileState(
    val isLoading: Boolean = false,
    val user: User? = null
) {
    val showUser: Boolean = (user != null) and !isLoading
}
