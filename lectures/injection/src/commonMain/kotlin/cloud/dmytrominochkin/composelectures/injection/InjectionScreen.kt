package cloud.dmytrominochkin.composelectures.injection

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.NavDeepLink
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import cloud.dmytrominochkin.composelectures.injection.ui.feed.Feed
import cloud.dmytrominochkin.composelectures.injection.ui.profile.Profile
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.app_name
import cloud.dmytrominochkin.composelectures.shared.resources.profile
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource

enum class AppScreens(val title: StringResource) {
    Feed(Res.string.app_name),
    Profile(Res.string.profile)
}

/** When we are using Jetpack Navigation component,
 * the ViewModels that are added here are going to be scoped to
 * the Navigation Graph destinations (or rather BackStackEntry).
 * If you are using Jetpack Navigation then using ViewModel removes more problems than it adds :)
 * So using them here is recommended.
 */
@Composable
fun InjectionScreen(
    modifier: Modifier = Modifier,
    navController: NavHostController = rememberNavController()
) {
    // Get current back stack
    val backStackState by navController.currentBackStackEntryAsState()
    // Get name of current screen
    val currentScreen = AppScreens.valueOf(
        backStackState?.destination?.route?.split("/")?.get(0) ?: AppScreens.Feed.name
    )
    val snackbarHostState = remember { SnackbarHostState() }
    Scaffold(
        modifier = modifier,
        topBar = {
            AppBarNav(
                title = currentScreen.title,
                canNavigateBack = navController.previousBackStackEntry != null,
                navigateUp = {
                    navController.navigateUp()
                }
            )
        },
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        }
    ) { innerPadding ->
        NavHost(
            navController = navController,
            startDestination = AppScreens.Feed.name,
            modifier = Modifier.padding(innerPadding)
        ) {
            composable(AppScreens.Feed.name) {
                // ViewModel here is scoped to this BackStackEntry.
                // While it remains in backstack - the viewmodel is alive
                // So when we press back - we already have feed list ready.
                // It's good practice to have a VM at the top destination of your graph/subgraph so
                // it can be reused in other screens of this subgraph if needed
                Feed(snackbarHostState, onSelected = { id ->
                    navController.navigate("${AppScreens.Profile.name}/$id")
                })
            }
            composable(
                "${AppScreens.Profile.name}/{userId}",
                arguments = listOf(
                    navArgument("userId") {
                        type = NavType.IntType
                    }
                ),
                deepLinks = listOf(
                    NavDeepLink.Builder().setUriPattern("lecture://profile/{userId}").build()
                )
            ) { backStackEntry ->
                backStackEntry.arguments?.getInt("userId")?.let { id ->
                    // ViewModel here is scoped to this BackStackEntry.
                    // When we press back - it is destroyed, as the entry is removed from stack.
                    // So we always get a new one when we visit this screen.
                    Profile(userId = id)
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun AppBarNav(
    title: StringResource,
    canNavigateBack: Boolean,
    navigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = { Text(stringResource(title)) },
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        modifier = modifier,
        navigationIcon = {
            if (canNavigateBack) {
                IconButton(onClick = navigateUp) {
                    Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack, contentDescription = "back")
                }
            }
        })
}
