package cloud.dmytrominochkin.composelectures.injection.di

import androidx.lifecycle.viewmodel.CreationExtras
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import cloud.dmytrominochkin.composelectures.injection.ui.feed.FeedViewModel
import cloud.dmytrominochkin.composelectures.injection.ui.profile.ProfileViewModel

/**
 * Provides Factory to create instance of ViewModel for the entire app
 */
internal object AppViewModelProvider {
    val Factory = viewModelFactory {
        /**
         * Initializer for [FeedViewModel]
         */
        initializer {
            FeedViewModel(socialApplication().appContainer.usersRepository)
        }
        /**
         * Initializer for [ProfileViewModel]
         */
        initializer {
            ProfileViewModel(socialApplication().appContainer.usersRepository)
        }
    }
}

/**
 * Extension function that queries for `android.app.Application` object and returns an instance of
 * [AppContainerProvider]. E.g.,
 * class MyApplication : Application(), AppContainerProvider {
 *
 *     override val appContainer: AppContainer by lazy {
 *         AppContainerImplementation()
 *     }
 */
internal expect fun CreationExtras.socialApplication(): AppContainerProvider
