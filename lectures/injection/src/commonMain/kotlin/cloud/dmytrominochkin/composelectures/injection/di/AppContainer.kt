package cloud.dmytrominochkin.composelectures.injection.di

import cloud.dmytrominochkin.composelectures.injection.datasource.MockNetDataSource
import cloud.dmytrominochkin.composelectures.injection.repository.MockUserRepository
import cloud.dmytrominochkin.composelectures.injection.repository.UserRepository

// App container for home-made dependency injection
interface AppContainer {
    val usersRepository: UserRepository
}

/**
 * [AppContainer] implementation that provides instance of [MockUserRepository]
 */
class AppDataContainer: AppContainer {
    override val usersRepository: UserRepository by lazy {
        MockUserRepository(MockNetDataSource())
    }

}

/**
 * You must implement [AppContainerProvider] on your Android `Application` class.
 */
interface AppContainerProvider {
    val appContainer: AppContainer
}