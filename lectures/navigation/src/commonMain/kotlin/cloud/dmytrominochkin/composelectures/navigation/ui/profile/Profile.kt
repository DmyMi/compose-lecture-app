package cloud.dmytrominochkin.composelectures.navigation.ui.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.navigation.model.User
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedPhotosCard
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedProfileHeader
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedTags
import org.jetbrains.compose.resources.painterResource


@Composable
internal fun Profile(
    user: User,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Surface(
        modifier = modifier,
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onSurface
    ) {
        Column(
//            Modifier.padding(top = dimensionResource(id = R.dimen.padding_large))
            Modifier.padding(top = 24.dp)
        ) {
            SharedProfileHeader(
                userName = user.name,
                numOfFollowers = user.numOfFollowers,
                numOfFollowing = user.numOfFollowing
            ) {
                Image(
                    painter = painterResource(resource = user.avatar),
                    contentDescription = "Avatar",
                    modifier = Modifier
                        .size(72.dp)
                        .clip(CircleShape),
                    contentScale = ContentScale.Crop
                )
            }
            SharedTags(
                tags = user.tags,
                Modifier.padding(top = mediumPadding, bottom = mediumPadding))
            SharedPhotosCard(groupedImages = user.photos) { image ->
                Image(
                    painterResource(resource = image),
                    modifier = Modifier
                        .aspectRatio(1f)
                        .clip(RoundedCornerShape(16.dp)),
                    contentDescription = "photo",
                    contentScale = ContentScale.Crop
                )
            }
        }
    }
}
