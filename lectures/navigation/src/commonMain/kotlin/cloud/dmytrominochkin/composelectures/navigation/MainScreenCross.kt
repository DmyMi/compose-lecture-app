package cloud.dmytrominochkin.composelectures.navigation

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.navigation.model.User
import cloud.dmytrominochkin.composelectures.navigation.ui.feed.Feed
import cloud.dmytrominochkin.composelectures.navigation.ui.profile.Profile
import cloud.dmytrominochkin.composelectures.shared.components.SharedBackHandler
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.profile
import org.jetbrains.compose.resources.stringResource


@Composable
internal fun MainScreenCross(
    modifier: Modifier = Modifier
) {
    var selectedId by rememberSaveable {
        mutableStateOf<Int?>(null)
    }
    Scaffold(
        modifier = modifier,
        topBar = {
            if (selectedId != null) {
                AppBarCross(navigateUp = { selectedId = null })
            }
        }
    ) { innerPadding ->
        Crossfade(
            targetState = selectedId,
            label = "crossfade",
            modifier = Modifier.padding(innerPadding)
        ) { id ->
            if (id == null) {
                Feed(users = User.userList, onSelected = { userId ->
                    selectedId = userId
                })
            } else {
                Profile(user = User.userList.first { user -> user.id == id })
                SharedBackHandler {
                    selectedId = null
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun AppBarCross(
    navigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = { Text(stringResource(Res.string.profile)) },
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        modifier = modifier,
        navigationIcon = {
            IconButton(onClick = navigateUp) {
                Icon(imageVector = Icons.AutoMirrored.Filled.ArrowBack, contentDescription = "back")
            }
        })
}
