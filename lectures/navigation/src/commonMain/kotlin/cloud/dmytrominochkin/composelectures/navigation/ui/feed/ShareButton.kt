package cloud.dmytrominochkin.composelectures.navigation.ui.feed

import androidx.compose.runtime.Composable

@Composable
internal expect fun ShareButton()