package cloud.dmytrominochkin.composelectures.navigation.ui.feed

import android.content.Intent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext

@Composable
internal actual fun ShareButton() {
    val context = LocalContext.current
    IconButton(onClick = {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            putExtra(Intent.EXTRA_SUBJECT, "Get social!")
            putExtra(Intent.EXTRA_TEXT, "Check this cool app!")
        }
        context.startActivity(
            Intent.createChooser(
                intent,
                "Share with SBU!"
            )
        )
    }) {
        Icon(Icons.Default.Share, contentDescription = "Share")
    }
}