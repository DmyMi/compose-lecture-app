package cloud.dmytrominochkin.composelectures.navigation.ui.feed

import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.navigation.model.User
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun FeedPreview() {
    ComposeLectureTheme {
        Feed(User.userList, {})
    }
}
