package cloud.dmytrominochkin.composelectures.room.api

import kotlinx.serialization.Serializable

@Serializable
internal data class ApiUser(
    val id: Int,
    val name: String,
    val avatar: String,
    val numOfFollowers: Int,
    val numOfFollowing: Int,
    val coverImage: String,
    val lastOnline: String,
    val tags: List<String>,
    val photos: Map<String, List<String>>,
)

@Serializable
internal data class ApiFeedUser(
    val id: Int,
    val name: String,
    val avatar: String,
    val numOfFollowers: Int,
    val numOfFollowing: Int,
    val coverImage: String,
    val lastOnline: String
)