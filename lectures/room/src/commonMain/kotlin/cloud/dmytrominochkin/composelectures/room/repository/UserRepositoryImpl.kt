package cloud.dmytrominochkin.composelectures.room.repository

import cloud.dmytrominochkin.composelectures.room.api.ApiFeedUser
import cloud.dmytrominochkin.composelectures.room.api.NetworkDataSource
import cloud.dmytrominochkin.composelectures.room.data.DataState
import cloud.dmytrominochkin.composelectures.room.db.AppPhoto
import cloud.dmytrominochkin.composelectures.room.db.DbUser
import cloud.dmytrominochkin.composelectures.room.db.GroupPhotosCrossRef
import cloud.dmytrominochkin.composelectures.room.db.PhotoGroup
import cloud.dmytrominochkin.composelectures.room.db.Tag
import cloud.dmytrominochkin.composelectures.room.db.UserDao
import cloud.dmytrominochkin.composelectures.room.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

internal class UserRepositoryImpl(
    private val api: NetworkDataSource,
    private val dao: UserDao
) : UserRepository {
    override fun getUser(id: Int): Flow<DataState<User>> = dao
        .getUserWithTagsAndPhotos(id)
        .map { dbUser ->
            val shouldFetch = dbUser.tags.isEmpty() or dbUser.groupedPhotos.isEmpty()
            if (shouldFetch) {
                DataState.Loading(dbUser.toUser())
                try {
                    val u = api.getUser(id).toUser()
                    val user = u.toDb()
                    val tags = u.tags.mapIndexed { i, t ->
                        Tag(
                            id = u.id * 10 + i + 1,
                            userId = u.id,
                            title = t
                        )
                    }
                    val groups = u.photos.keys.mapIndexed { i, g ->
                        PhotoGroup(
                            groupId = u.id * 10 + i + 1,
                            userId = u.id,
                            title = g
                        )
                    }
                    val photos = groups.flatMap { g ->
                        u.photos[g.title]!!.mapIndexed { i, photo ->
                            AppPhoto(photoId = g.groupId * 10 + i + 1, stored = photo)
                        }
                    }
                    val groupedPhotos = groups.flatMap { g ->
                        List(u.photos[g.title]!!.size) { i ->
                            GroupPhotosCrossRef(
                                photoId = g.groupId * 10 + i + 1,
                                groupId = g.groupId
                            )
                        }
                    }
                    dao.addUserWithTagsGroupsPhotos(user, tags, groups, photos, groupedPhotos)
                    DataState.Success(u)
                } catch (t: Throwable) {
                    DataState.Error(t)
                }
            } else {
                DataState.Success(dbUser.toUser())
            }
        }
        .flowOn(Dispatchers.IO)

    override fun loadFeed(): Flow<DataState<List<User>>> = flow {
        val cached = dao.getFeed().map(DbUser::toUser)
        emit(DataState.Loading(cached))

        try {
            val response = api.loadFeed()
            // it's better not to convert api models to entities directly :)
            dao.addUsers(response.map(ApiFeedUser::toDb))
            // according to algorithm is better to load from DB again, but im lazy
            emit(DataState.Success(response.map(ApiFeedUser::toUser)))
        } catch (t: Throwable) {
            emit(DataState.Error(t))
        }
    }
        .flowOn(Dispatchers.IO)

    override suspend fun follow(id: Int) =
        withContext(Dispatchers.IO) {
            val user = dao.getUser(id)
            dao.updateUser(user.copy(numOfFollowing = user.numOfFollowing.inc()))
        }
}
