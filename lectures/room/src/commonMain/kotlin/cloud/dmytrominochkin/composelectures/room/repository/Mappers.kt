package cloud.dmytrominochkin.composelectures.room.repository

import cloud.dmytrominochkin.composelectures.room.api.ApiFeedUser
import cloud.dmytrominochkin.composelectures.room.api.ApiUser
import cloud.dmytrominochkin.composelectures.room.db.DbUser
import cloud.dmytrominochkin.composelectures.room.db.UserWithTagsAndPhotos
import cloud.dmytrominochkin.composelectures.room.model.User

internal fun ApiFeedUser.toUser() = User(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline,
    tags = emptyList(),
    photos = emptyMap()
)

internal fun ApiFeedUser.toDb() = DbUser(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline
)

internal fun DbUser.toUser() = User(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline,
    tags = emptyList(),
    photos = emptyMap()
)

internal fun User.toDb() = DbUser(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline
)

internal fun UserWithTagsAndPhotos.toUser() =
    User(
        id = user.id,
        name = user.name,
        avatar = user.avatar,
        numOfFollowers = user.numOfFollowers,
        numOfFollowing = user.numOfFollowing,
        coverImage = user.coverImage,
        lastOnline = user.lastOnline,
        tags = tags.map { it.title },
        photos = groupedPhotos.associate { gp ->
            gp.group.title to gp.photos.map { p -> p.stored }
        }
    )

internal fun ApiUser.toUser() = User(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline,
    tags = tags,
    photos = photos
)
