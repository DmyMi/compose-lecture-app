package cloud.dmytrominochkin.composelectures.room.di

import cloud.dmytrominochkin.composelectures.room.api.NetworkDataSource
import cloud.dmytrominochkin.composelectures.room.db.SocialDatabase
import cloud.dmytrominochkin.composelectures.room.repository.UserRepository
import cloud.dmytrominochkin.composelectures.room.repository.UserRepositoryImpl
import cloud.dmytrominochkin.composelectures.room.ui.feed.FeedViewModel
import cloud.dmytrominochkin.composelectures.room.ui.profile.ProfileViewModel
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.viewModelOf
import org.koin.dsl.bind
import org.koin.dsl.module

internal expect val dbModule: org.koin.core.module.Module

internal val appModule = module {

    single {
        val db: SocialDatabase = get()
        db.userDao()
    }

    single {
        HttpClient(OkHttp) {
            install(ContentNegotiation) {
                json(Json {
                    ignoreUnknownKeys = true
                })
            }
        }
    }
    singleOf(::NetworkDataSource)
    singleOf(::UserRepositoryImpl) bind UserRepository::class

    viewModelOf(::FeedViewModel)
    viewModelOf(::ProfileViewModel)
}

fun roomModules() = listOf(dbModule, appModule)
