package cloud.dmytrominochkin.composelectures.room.db

import androidx.room.ConstructedBy
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.RoomDatabaseConstructor
import androidx.sqlite.driver.bundled.BundledSQLiteDriver
import cloud.dmytrominochkin.composelectures.room.SocialAppContext
import kotlinx.coroutines.Dispatchers

@Database(
    entities = [
        DbUser::class,
        Tag::class,
        PhotoGroup::class,
        AppPhoto::class,
        GroupPhotosCrossRef::class
    ],
    version = 1,
    exportSchema = false
)
@ConstructedBy(SocialDatabaseConstructor::class)
internal abstract class SocialDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
}

// The Room compiler generates the `actual` implementations.
@Suppress("NO_ACTUAL_FOR_EXPECT")
internal expect object SocialDatabaseConstructor : RoomDatabaseConstructor<SocialDatabase>

internal fun getRoomDatabase(
    builder: RoomDatabase.Builder<SocialDatabase>
): SocialDatabase {
    return builder
        .setDriver(BundledSQLiteDriver())
        .setQueryCoroutineContext(Dispatchers.IO)
        .build()
}

internal expect fun getDatabaseBuilder(context: SocialAppContext): RoomDatabase.Builder<SocialDatabase>