package cloud.dmytrominochkin.composelectures.room.api

import cloud.dmytrominochkin.composelectures.room.BASE_URL
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

internal class NetworkDataSource(
    private val client: HttpClient
) {
    suspend fun loadFeed(): List<ApiFeedUser> =
        client.get("${BASE_URL}/users").body()

    suspend fun getUser(id: Int): ApiUser =
        client.get("${BASE_URL}/users/${id}").body()
}