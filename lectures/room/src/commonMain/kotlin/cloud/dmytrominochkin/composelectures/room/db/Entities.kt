package cloud.dmytrominochkin.composelectures.room.db

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Junction
import androidx.room.PrimaryKey
import androidx.room.Relation

@Entity(tableName = "User")
data class DbUser(
    @PrimaryKey
    val id: Int,
    val name: String,
    val avatar: String,
    val numOfFollowers: Int,
    val numOfFollowing: Int,
    val lastOnline: String,
    val coverImage: String,
)

@Entity
data class Tag(
    @PrimaryKey
    val id: Int,
    val userId: Int,
    val title: String
)

@Entity
data class PhotoGroup(
    @PrimaryKey
    val groupId: Int,
    val userId: Int,
    val title: String
)

@Entity
data class AppPhoto(
    @PrimaryKey
    val photoId: Int,
    val stored: String
)

@Entity(primaryKeys = ["groupId", "photoId"])
data class GroupPhotosCrossRef(
    val groupId: Int,
    val photoId: Int
)

data class GroupWithPhotos(
    @Embedded
    val group: PhotoGroup,
    @Relation(
        parentColumn = "groupId",
        entityColumn = "photoId",
        associateBy = Junction(GroupPhotosCrossRef::class)
    )
    val photos: List<AppPhoto>
)

data class UserWithTagsAndPhotos(
    @Embedded
    val user: DbUser,
    @Relation(
        entity = Tag::class,
        parentColumn = "id",
        entityColumn = "userId"
    )
    val tags: List<Tag>,
    @Relation(
        entity = PhotoGroup::class,
        parentColumn = "id",
        entityColumn = "userId"
    )
    val groupedPhotos: List<GroupWithPhotos>
)