package cloud.dmytrominochkin.composelectures.room.ui.profile

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cloud.dmytrominochkin.composelectures.room.data.DataState
import cloud.dmytrominochkin.composelectures.room.data.data
import cloud.dmytrominochkin.composelectures.room.model.User
import cloud.dmytrominochkin.composelectures.room.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

// For a simple application like this it's an overkill to have 2 viewmodels.
// One is more then enough to handle this flow. Just for demo purposes
internal class ProfileViewModel(
    private val usersRepository: UserRepository
) : ViewModel() {
    private val _uiState = MutableStateFlow(ProfileState())
    val uiState = _uiState.asStateFlow()

    fun getUser(id: Int) {
        usersRepository
            .getUser(id)
            .onEach { dataState ->
                when(dataState) {
                    is DataState.Loading -> _uiState.update { oldState -> oldState.copy(isLoading = true, user = dataState.data) }
                    is DataState.Error -> _uiState.update { it.copy(isLoading = false) }
                    is DataState.Success -> _uiState.update { oldState -> oldState.copy(isLoading = false, user = dataState.data) }
                }
            }
            .launchIn(viewModelScope)
    }

    fun follow() {
        _uiState.value.user?.id?.let { id ->
            viewModelScope.launch {
                usersRepository.follow(id)
            }
        }
    }
}

internal data class ProfileState(
    val isLoading: Boolean = false,
    val user: User? = null
) {
    val showUser: Boolean = (user != null) and !isLoading
}

