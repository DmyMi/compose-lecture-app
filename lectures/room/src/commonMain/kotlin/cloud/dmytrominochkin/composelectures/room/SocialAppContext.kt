package cloud.dmytrominochkin.composelectures.room

/**
 * Represents a platform-specific context that acts as an interface to
 * global information about an application environment.
 */
internal expect abstract class SocialAppContext