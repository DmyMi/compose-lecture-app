package cloud.dmytrominochkin.composelectures.room.repository

import cloud.dmytrominochkin.composelectures.room.data.DataState
import cloud.dmytrominochkin.composelectures.room.model.User

import kotlinx.coroutines.flow.Flow

internal interface UserRepository {
    fun getUser(id: Int): Flow<DataState<User>>
    fun loadFeed(): Flow<DataState<List<User>>>
    suspend fun follow(id: Int)
}
