package cloud.dmytrominochkin.composelectures.room.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
internal interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUsers(users: List<DbUser>)

    @Update
    suspend fun updateUser(user: DbUser)

    @Query("SELECT * FROM User")
    suspend fun getFeed(): List<DbUser>

    @Query("SELECT * FROM User WHERE id = :id")
    suspend fun getUser(id: Int): DbUser

    @Transaction
    @Query("SELECT * FROM User WHERE id = :id")
    fun getUserWithTagsAndPhotos(id: Int): Flow<UserWithTagsAndPhotos>

    @Transaction
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUserWithTagsGroupsPhotos(user: DbUser, tags: List<Tag>, groups: List<PhotoGroup>, photos: List<AppPhoto>, groupedPhotos: List<GroupPhotosCrossRef>)
}