package cloud.dmytrominochkin.composelectures.room.data

import cloud.dmytrominochkin.composelectures.room.model.User

internal val users = mutableListOf(
    User(
        1,
        "Mary Jane",
        "/images/user/avatar_1.jpg",
        1000,
        500,
        "/images/user/image_1.jpeg",
        "3 minutes ago",
        listOf(
            "food",
            "fashion",
            "nature",
            "dogs",
            "people",
            "selfies",
            "style",
            "fashion",
            "cats"
        ),
        mapOf(
            "Today" to listOf(
                "/images/user/user1_1.jpg",
                "/images/user/user1_2.jpg",
                "/images/user/user1_3.jpg",
                "/images/user/user1_4.jpg",
                "/images/user/user1_5.jpg",
                "/images/user/user1_6.jpg",
            ),
            "Hobby" to listOf(
                "/images/user/user2_1.jpg",
                "/images/user/user2_2.jpg",
                "/images/user/user2_3.jpg",
                "/images/user/user2_4.jpg",
                "/images/user/user2_5.jpg",
                "/images/user/user2_6.jpg",
            )
        )
    ),
    User(
        2,
        "Hugh Jass",
        "/images/user/avatar_2.jpeg",
        2000,
        999,
        "/images/user/image_2.png",
        "10 minutes ago",
        listOf("people", "selfies", "style", "fashion"),
        mapOf(
            "Vacation" to listOf(
                "/images/user/user1_1.jpg",
                "/images/user/user1_2.jpg",
                "/images/user/user1_3.jpg",
                "/images/user/user1_4.jpg",
                "/images/user/user1_5.jpg",
                "/images/user/user1_6.jpg",
            ),
            "Today" to listOf(
                "/images/user/user2_1.jpg",
                "/images/user/user2_2.jpg",
                "/images/user/user2_3.jpg",
                "/images/user/user2_4.jpg",
                "/images/user/user2_5.jpg",
                "/images/user/user2_6.jpg",
            ),
            "Inspiration" to listOf(
                "/images/user/user3_1.jpg",
                "/images/user/user3_2.jpg",
                "/images/user/user3_3.jpg",
                "/images/user/user3_4.jpg",
                "/images/user/user3_5.jpg",
                "/images/user/user3_6.jpg",
            )
        )
    ),
    User(
        3,
        "Tess Tickle",
        "/images/user/avatar_3.jpeg",
        69,
        420,
        "/images/user/image_3.jpg",
        "1 day ago",
        listOf("selife", "cats", "nature", "fashion"),
        mapOf(
            "Life" to listOf(
                "/images/user/user3_1.jpg",
                "/images/user/user3_2.jpg",
                "/images/user/user3_3.jpg",
                "/images/user/user3_4.jpg",
                "/images/user/user3_5.jpg",
                "/images/user/user3_6.jpg",
            ),
            "Hobby" to listOf(
                "/images/user/user1_1.jpg",
                "/images/user/user1_2.jpg",
                "/images/user/user1_3.jpg",
                "/images/user/user1_4.jpg",
                "/images/user/user1_5.jpg",
                "/images/user/user1_6.jpg",
            )
        )
    )
)
