package cloud.dmytrominochkin.composelectures.room.db

import androidx.room.Room
import androidx.room.RoomDatabase
import cloud.dmytrominochkin.composelectures.room.SocialAppContext

internal actual fun getDatabaseBuilder(context: SocialAppContext): RoomDatabase.Builder<SocialDatabase> {
    val appContext = context.applicationContext
    val dbFile = appContext.getDatabasePath("social.db")
    return Room.databaseBuilder<SocialDatabase>(
        context = appContext,
        name = dbFile.absolutePath
    )
}