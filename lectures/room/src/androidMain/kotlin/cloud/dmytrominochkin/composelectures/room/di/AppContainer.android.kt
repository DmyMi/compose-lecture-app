package cloud.dmytrominochkin.composelectures.room.di

import cloud.dmytrominochkin.composelectures.room.db.getDatabaseBuilder
import cloud.dmytrominochkin.composelectures.room.db.getRoomDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.core.module.Module
import org.koin.dsl.module

internal actual val dbModule: Module = module {
    single { getRoomDatabase(getDatabaseBuilder(androidContext())) }
}