package cloud.dmytrominochkin.composelectures.room

internal actual abstract class SocialAppContext private constructor() {
    companion object {
        @JvmField
        val INSTANCE = object : SocialAppContext() {}
    }
}