package cloud.dmytrominochkin.composelectures.room.db

import androidx.room.Room
import androidx.room.RoomDatabase
import cloud.dmytrominochkin.composelectures.room.SocialAppContext
import java.io.File

internal actual fun getDatabaseBuilder(context: SocialAppContext): RoomDatabase.Builder<SocialDatabase> {
    val dbFile = File(System.getProperty("java.io.tmpdir"), "social.db")
    return Room.databaseBuilder<SocialDatabase>(
        name = dbFile.absolutePath,
    )
}