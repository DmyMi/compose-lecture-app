package cloud.dmytrominochkin.composelectures.state

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme
import cloud.dmytrominochkin.composelectures.state.model.User
import cloud.dmytrominochkin.composelectures.state.ui.profile.Profile

@TranslationPreview
@Composable
internal fun ProfilePreview() {
    ComposeLectureTheme {
        Surface {
            Profile(User.example, Modifier.fillMaxSize())
        }
    }
}
