package cloud.dmytrominochkin.composelectures.state

import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme
import cloud.dmytrominochkin.composelectures.state.ui.dialog.UsernameDialog

@TranslationPreview
@Composable
internal fun UsernameDialogPreview() {
    ComposeLectureTheme {
        UsernameDialog("", {}, {}, {})
    }
}
