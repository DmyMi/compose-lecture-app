package cloud.dmytrominochkin.composelectures.state

import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme
import cloud.dmytrominochkin.composelectures.state.model.User
import cloud.dmytrominochkin.composelectures.state.ui.feed.FeedItem

@TranslationPreview
@Composable
internal fun FeedItemPreview() {
    ComposeLectureTheme {
        Surface {
            FeedItem(user = User.example, onClick = {})
        }
    }
}
