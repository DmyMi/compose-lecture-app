package cloud.dmytrominochkin.composelectures.state.ui.feed

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.state.model.User
import org.jetbrains.compose.resources.painterResource

@Composable
internal fun FeedItem(
    user: User,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Column(
        modifier = modifier
            .clickable(onClick = onClick)
            .padding(
                top = mediumPadding / 2,
                bottom = mediumPadding / 2,
                start = mediumPadding,
                end = mediumPadding
            )
            .fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(resource = user.avatar),
                contentDescription = "Avatar",
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape),
                contentScale = ContentScale.Crop
            )
            Spacer(modifier = Modifier.padding(mediumPadding))
            Column {
                Text(
                    text = user.name,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Medium)
                )
                Text(
                    text = user.lastOnline,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Light)
                )
            }
        }
        Spacer(modifier = Modifier.padding(mediumPadding))
        Image(
            painter = painterResource(resource = user.coverImage),
            contentDescription = "cover",
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
                .shadow(elevation = 4.dp, clip = true, shape = RoundedCornerShape(4.dp)),
            contentScale = ContentScale.Crop
        )
    }
}
