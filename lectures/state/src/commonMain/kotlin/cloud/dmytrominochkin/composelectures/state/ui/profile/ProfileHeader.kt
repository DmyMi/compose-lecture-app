package cloud.dmytrominochkin.composelectures.state.ui.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedFollowersInfo
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.follow
import cloud.dmytrominochkin.composelectures.shared.resources.followers
import cloud.dmytrominochkin.composelectures.shared.resources.following
import cloud.dmytrominochkin.composelectures.state.model.User
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource

@Composable
internal fun ProfileHeader(
    user: User,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .padding(start = mediumPadding, end = mediumPadding)
    ) {
        Image(
            painter = painterResource(resource = user.avatar),
            contentDescription = "Avatar",
            modifier = Modifier
                .size(72.dp)
                .clip(CircleShape),
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.size(mediumPadding))
        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(text = user.name, style = MaterialTheme.typography.titleLarge)
            Row (
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                SharedFollowersInfo(number = user.numOfFollowers, text = stringResource(Res.string.followers))
                SharedFollowersInfo(number = user.numOfFollowing, text = stringResource(Res.string.following))
                Button(onClick = { }, shape = CircleShape) {
                    Text(text = stringResource(Res.string.follow))
                }
            }
        }
    }
}
