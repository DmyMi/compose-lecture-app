package cloud.dmytrominochkin.composelectures.state.ui.feed

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
internal fun FeedFab(
    state: LazyListState,
    onClick: () -> Unit,
    modifier: Modifier
) {
    val isVisible by remember {
        derivedStateOf {
            !state.isScrollInProgress
        }
    }

    AnimatedVisibility(
        visible = isVisible,
        modifier = modifier,
        enter = fabEnterAnim,
        exit = fabExitAnim
    ) {
        FloatingActionButton(
            onClick = onClick,
            modifier = modifier.padding(8.dp),
            containerColor = MaterialTheme.colorScheme.primary
        ) {
            Icon(Icons.Default.AccountCircle, contentDescription = null)
        }
    }
}

private val fabEnterAnim = slideInVertically(initialOffsetY = { it })
private val fabExitAnim = slideOutVertically(targetOffsetY = { it })
