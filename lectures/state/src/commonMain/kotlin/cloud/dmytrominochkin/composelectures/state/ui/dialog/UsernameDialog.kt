package cloud.dmytrominochkin.composelectures.state.ui.dialog

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.cancel
import cloud.dmytrominochkin.composelectures.shared.resources.not_a_real
import cloud.dmytrominochkin.composelectures.shared.resources.save
import cloud.dmytrominochkin.composelectures.shared.resources.your_name
import org.jetbrains.compose.resources.stringResource


@Composable
internal fun UsernameDialog(
    name: String,
    onNameChange: (String) -> Unit,
    onDismiss: () -> Unit,
    onConfirm: () -> Unit
) {
//    val padding = dimensionResource(id = R.dimen.padding_medium)
    val padding = 16.dp
//    val smallPadding = dimensionResource(id = R.dimen.padding_small)
    val smallPadding = 8.dp
    Dialog(onDismissRequest = onDismiss) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .height(375.dp)
                .padding(padding),
            shape = RoundedCornerShape(padding),
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Icon(
                    Icons.Default.AccountCircle,
                    contentDescription = null,
                    modifier = Modifier.size(100.dp)
                )
                TextField(
                    value = name,
                    onValueChange = onNameChange,
                    modifier = Modifier.padding(padding),
                    placeholder = { Text(stringResource(Res.string.your_name))}
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                ) {
                    TextButton(
                        onClick = onDismiss,
                        modifier = Modifier.padding(smallPadding),
                    ) {
                        Text(stringResource(Res.string.cancel), style = MaterialTheme.typography.titleLarge)
                    }
                    TextButton(
                        onClick = onConfirm,
                        modifier = Modifier.padding(smallPadding),
                    ) {
                        Text(stringResource(Res.string.save), style = MaterialTheme.typography.titleLarge)
                    }
                }
                Text(
                    text = stringResource(Res.string.not_a_real),
                    style = MaterialTheme.typography.labelSmall,
                    fontWeight = FontWeight.Light)
            }
        }
    }
}
