package cloud.dmytrominochkin.composelectures.state

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.shared.ViewSwitch
import cloud.dmytrominochkin.composelectures.shared.rememberViewSwitchState
import cloud.dmytrominochkin.composelectures.state.model.User
import cloud.dmytrominochkin.composelectures.state.ui.feed.Feed
import cloud.dmytrominochkin.composelectures.state.ui.profile.Profile

@Composable
fun StateScreen(
    modifier: Modifier = Modifier
) {
    val checkedState = rememberViewSwitchState()
    Column(modifier = modifier) {
        ViewSwitch(checkedState, "Feed", "Profile")
        if (checkedState.isChecked) {
            Profile(User.example)
        } else {
            Feed(User.userList)
        }
    }
}