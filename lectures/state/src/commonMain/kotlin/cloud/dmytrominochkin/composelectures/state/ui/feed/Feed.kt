package cloud.dmytrominochkin.composelectures.state.ui.feed

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.state.model.User
import cloud.dmytrominochkin.composelectures.state.ui.dialog.UsernameDialog
import kotlinx.coroutines.launch

@Composable
internal fun Feed(
    users: List<User>,
    modifier: Modifier = Modifier
) {
    var isDialogVisible by remember {
        mutableStateOf(false)
    }
    var name by rememberSaveable {
        mutableStateOf("")
    }

    val snackbarHostState = remember { SnackbarHostState() }
    val coroutineScope = rememberCoroutineScope()

    Surface(modifier = modifier.fillMaxSize()) {
        val state = rememberLazyListState()
        LazyColumn(state = state) {
            item {
                FeedHeader(name)
            }
            items(users) { user ->
                FeedItem(user = user, onClick = {
                    // TODO: add navigation
                })
            }
        }
        FeedFab(
            state = state,
            onClick = { isDialogVisible = true },
            modifier = Modifier.wrapContentSize(align = Alignment.BottomEnd)
        )
        if (isDialogVisible) {
            UsernameDialog(
                name = name,
                onNameChange = { newName ->
                    name = newName
                },
                onDismiss = {
                    name = ""
                    isDialogVisible = false
                },
                onConfirm = {
                    coroutineScope.launch {
                        snackbarHostState.showSnackbar(message = "You are logged in as $name", duration = SnackbarDuration.Short)
                    }
                    isDialogVisible = false
                }
            )
        }
    }
}
