package cloud.dmytrominochkin.composelectures.state.ui.profile

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@OptIn(ExperimentalLayoutApi::class)
@Composable
internal fun Tags(tags: List<String>, modifier: Modifier = Modifier) {
//    val padding = dimensionResource(id = R.dimen.padding_small)
    val padding = 8.dp
    FlowRow(
        modifier = modifier
            .padding(PaddingValues(start = padding * 2, end = padding)),
        horizontalArrangement = Arrangement.spacedBy(padding)
    ) {
        tags.forEach { tag ->
            Text(
                text = tag,
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier
                    .border(
                        1.dp,
                        MaterialTheme.colorScheme.onSurface.copy(alpha = 0.38f),
                        CircleShape
                    )
                    .padding(padding)
            )
        }
    }
}
