package cloud.dmytrominochkin.composelectures.state.ui.profile

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.photos
import org.jetbrains.compose.resources.DrawableResource
import org.jetbrains.compose.resources.stringResource

@Composable
internal fun PhotosCard(
    images: List<DrawableResource>,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Surface(
        modifier = modifier.fillMaxWidth(),
        shadowElevation = 2.dp,
        shape = RoundedCornerShape(10, 10, 0, 0)
    ) {
        Column {
            Text(
                text = stringResource(Res.string.photos),
                style = MaterialTheme.typography.headlineSmall,
                modifier = Modifier.padding(
                    start = mediumPadding,
                    end = mediumPadding,
                    top = mediumPadding
                )
            )
            PhotosTab()
            PhotosGrid(
                images = images,
                modifier = Modifier
                    .padding(
                        top = mediumPadding / 2,
                        start = mediumPadding,
                        end = mediumPadding,
                        bottom = mediumPadding
                    )
            )
        }
    }
}
