package cloud.dmytrominochkin.composelectures.resources

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.resources.model.User
import cloud.dmytrominochkin.composelectures.resources.ui.feed.FeedItem
import cloud.dmytrominochkin.composelectures.resources.ui.profile.Profile
import cloud.dmytrominochkin.composelectures.shared.ViewSwitch
import cloud.dmytrominochkin.composelectures.shared.rememberViewSwitchState

@Composable
fun ResourcesScreen(
    modifier: Modifier = Modifier
) {
    val checkedState = rememberViewSwitchState()
    Column(modifier = modifier) {
        ViewSwitch(checkedState, "Feed", "Profile")
        if (checkedState.isChecked) {
            Profile(User.example)
        } else {
            FeedItem(User.example, {})
        }
    }
}