package cloud.dmytrominochkin.composelectures.resources.ui.feed

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.resources.LocalDimensions
import cloud.dmytrominochkin.composelectures.resources.model.User
import org.jetbrains.compose.resources.painterResource

/**
 * Displays a feed item consisting of user information and a cover image.
 *
 * [clickable] Modifier:
 * - The `.clickable` modifier makes the entire component respond to click events.
 * - When the user taps the feed item, the `onClick` lambda is invoked.
 *
 * [FontWeight]:
 * - An enum class that defines the weight (thickness) of the font, such as [FontWeight.Medium] or [FontWeight.Light].
 * - Used to emphasize text by making it bolder or lighter.
 *
 * [shadow] Modifier**:
 * - The `.shadow` modifier adds a drop shadow to the component.
 * - Parameters include `elevation`, `clip`, and `shape`, allowing customization of the shadow's appearance.
 *
 * [RoundedCornerShape]:
 * - A shape that rounds the corners of a component.
 * - Can specify different corner radii for each corner or a uniform radius for all corners.
 * - Used with modifiers like `.clip` or `.shadow` to apply the shape to composables.
 *
 * @param user The [User] whose feed item is being displayed.
 * @param onClick A lambda function invoked when the feed item is clicked.
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 */
@Composable
internal fun FeedItem(
    user: User,
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalDimensions.current.paddingMedium
    Column(
        modifier = modifier
            .clickable(onClick = onClick)
            .padding(
                top = mediumPadding / 2,
                bottom = mediumPadding / 2,
                start = mediumPadding,
                end = mediumPadding
            )
            .fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(resource = user.avatar),
                contentDescription = "Avatar",
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape),
                contentScale = ContentScale.Crop
            )
            Spacer(modifier = Modifier.padding(mediumPadding))
            Column {
                Text(
                    text = user.name,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Medium)
                )
                Text(
                    text = user.lastOnline,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Light)
                )
            }
        }
        Spacer(modifier = Modifier.padding(mediumPadding))
        Image(
            painter = painterResource(resource = user.coverImage),
            contentDescription = "cover",
            modifier = Modifier
                .fillMaxWidth()
                .height(250.dp)
                .shadow(elevation = 4.dp, clip = true, shape = RoundedCornerShape(4.dp)),
            contentScale = ContentScale.Crop
        )
    }
}
