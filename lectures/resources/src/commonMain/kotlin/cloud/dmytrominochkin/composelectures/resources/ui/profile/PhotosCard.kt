package cloud.dmytrominochkin.composelectures.resources.ui.profile

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.resources.LocalDimensions
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.photos
import org.jetbrains.compose.resources.stringResource

/**
 * Displays a card containing the user's photos, with a header and a placeholder for the photo list.
 *
 * This is a [Composable] function in Jetpack Compose that constructs a UI card component.
 *
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 */
@Composable
internal fun PhotosCard(
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalDimensions.current.paddingMedium
    Surface(
        modifier = modifier.fillMaxWidth(),
        shadowElevation = 2.dp,
        shape = RoundedCornerShape(20, 20, 0, 0)
    ) {
        Column {
            Text(
                text = stringResource(Res.string.photos),
                style = MaterialTheme.typography.headlineSmall,
                modifier = Modifier.padding(start = mediumPadding, end = mediumPadding, top = mediumPadding)
            )
            // TODO: add list of user photos
        }
    }
}
