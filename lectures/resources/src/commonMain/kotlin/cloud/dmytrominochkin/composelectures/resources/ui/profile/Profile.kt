package cloud.dmytrominochkin.composelectures.resources.ui.profile

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.resources.LocalDimensions
import cloud.dmytrominochkin.composelectures.resources.model.User

/**
 * Displays the user's profile screen, including the profile header and photos card.
 *
 * This is a [Composable] function in Jetpack Compose that constructs the UI for the profile screen.
 *
 * @param user The [User] whose profile information is displayed.
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 */
@Composable
internal fun Profile(user: User, modifier: Modifier = Modifier) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalDimensions.current.paddingMedium
    Surface(
        modifier = modifier,
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onSurface
    ) {
        Column(
            Modifier.padding(top = LocalDimensions.current.paddingLarge)
//            Modifier.padding(top = dimensionResource(id = R.dimen.padding_large))
        ) {
            ProfileHeader(user = user)
            PhotosCard()
        }
    }
}
