package cloud.dmytrominochkin.composelectures.resources.ui.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.resources.LocalDimensions
import cloud.dmytrominochkin.composelectures.resources.model.User
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.follow
import cloud.dmytrominochkin.composelectures.shared.resources.followers
import cloud.dmytrominochkin.composelectures.shared.resources.following
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource

/**
 * Displays a user's profile header, including avatar, name, follower counts, and a follow button.
 *
 * [ContentScale.Crop]:
 * - An option for scaling the content of an [Image].
 * - Scales the image uniformly, maintaining the aspect ratio, so that both dimensions of the image
 *   will be equal to or larger than the corresponding dimension of the view.
 * - The image is centered within the view, and any parts that lie outside the view are cropped.
 *
 * [MaterialTheme.typography]:
 * - Part of the [MaterialTheme] in Jetpack Compose, which provides theming and styling.
 * - [Typography] defines a set of text styles used throughout the app.
 * - `titleLarge` is a predefined text style for large titles, ensuring consistency in text appearance.
 *
 * [Button]:
 * - A composable that represents a Material Design button.
 * - Used to capture user interactions and trigger actions when pressed.
 * - Can be customized with text, icons, shapes, colors, and more.
 *
 * [stringResource]:
 * - A function that retrieves a localized string resource.
 * - Facilitates internationalization by supporting multiple languages.
 * - Fetches strings defined in resource files, allowing for easy management and updates.
 *
 * [Res.string]:
 * - An object that holds references to string resources.
 * - Contains identifiers like `Res.string.followers`, `Res.string.following`, and `Res.string.follow`,
 *   which correspond to localized strings defined in the resources.
 *
 * @param user The [User] whose profile information is displayed.
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 */
@Composable
internal fun ProfileHeader(
    user: User,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalDimensions.current.paddingMedium
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .padding(start = mediumPadding, end = mediumPadding)
    ) {
        Image(
            painter = painterResource(resource = user.avatar),
            contentDescription = "Avatar",
            modifier = Modifier
                .size(72.dp)
                .clip(CircleShape),
            contentScale = ContentScale.Crop
        )
        Spacer(modifier = Modifier.size(mediumPadding))
        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(text = user.name, style = MaterialTheme.typography.titleLarge)
            Row (
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                FollowersInfo(number = user.numOfFollowers, text = stringResource(Res.string.followers))
                FollowersInfo(number = user.numOfFollowing, text = stringResource(Res.string.following))
                Button(onClick = { }, shape = CircleShape) {
                    Text(text = stringResource(Res.string.follow))
                }
            }
        }
    }
}
