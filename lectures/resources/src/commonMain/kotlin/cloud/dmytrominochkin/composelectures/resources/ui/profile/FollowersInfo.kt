package cloud.dmytrominochkin.composelectures.resources.ui.profile

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

@Composable
internal fun FollowersInfo(number: Int, text: String, modifier: Modifier = Modifier) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Text(text = number.toString(), style = MaterialTheme.typography.titleSmall)
        Text(text = text, style = MaterialTheme.typography.labelMedium)
    }
}
