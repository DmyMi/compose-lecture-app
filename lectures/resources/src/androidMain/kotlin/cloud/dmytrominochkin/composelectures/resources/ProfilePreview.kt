package cloud.dmytrominochkin.composelectures.resources

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.resources.model.User
import cloud.dmytrominochkin.composelectures.resources.ui.profile.Profile
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun ProfilePreview() {
    ComposeLectureTheme {
        Surface {
            Profile(User.example, Modifier.fillMaxSize())
        }
    }
}
