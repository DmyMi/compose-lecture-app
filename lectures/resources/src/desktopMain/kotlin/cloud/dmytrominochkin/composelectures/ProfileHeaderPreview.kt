package cloud.dmytrominochkin.composelectures

import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.resources.model.User
import cloud.dmytrominochkin.composelectures.resources.ui.profile.ProfileHeader
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme


@TranslationPreview
@Composable
internal fun ProfileHeaderPreview() {
    ComposeLectureTheme {
        Surface {
            ProfileHeader(user = User.example)
        }
    }
}
