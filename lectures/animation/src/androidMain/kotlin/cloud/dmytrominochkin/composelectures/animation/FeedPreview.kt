package cloud.dmytrominochkin.composelectures.animation

import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.animation.model.User
import cloud.dmytrominochkin.composelectures.animation.ui.feed.Feed
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun FeedPreview() {
    ComposeLectureTheme {
        Feed(User.userList, {})
    }
}
