package cloud.dmytrominochkin.composelectures.animation

import androidx.compose.runtime.Composable
import androidx.activity.compose.BackHandler as AndroidBackHandler

@Composable
internal actual fun BackHandler(isEnabled: Boolean, onBack: () -> Unit) {
    AndroidBackHandler(isEnabled, onBack)
}