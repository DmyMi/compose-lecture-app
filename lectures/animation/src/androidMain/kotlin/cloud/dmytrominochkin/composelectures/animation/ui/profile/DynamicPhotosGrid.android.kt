package cloud.dmytrominochkin.composelectures.animation.ui.profile

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.runtime.Composable

@Composable
internal actual fun BoxScope.VerticalScrollbar(scrollState: ScrollState) {
}