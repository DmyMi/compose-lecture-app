package cloud.dmytrominochkin.composelectures.animation

import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.animation.model.User
import cloud.dmytrominochkin.composelectures.animation.ui.profile.ProfileHeader
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun ProfileHeaderPreview() {
    ComposeLectureTheme {
        Surface {
            ProfileHeader(user = User.example)
        }
    }
}
