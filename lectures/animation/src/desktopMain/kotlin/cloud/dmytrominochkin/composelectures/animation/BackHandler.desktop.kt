package cloud.dmytrominochkin.composelectures.animation

import androidx.compose.runtime.Composable

@Composable
actual fun BackHandler(isEnabled: Boolean, onBack: () -> Unit) {
    // Do nothing, as Desktop has no concept of back event
}