package cloud.dmytrominochkin.composelectures.animation.ui.profile

import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDp
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Tab
import androidx.compose.material3.TabPosition
import androidx.compose.material3.TabRow
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp

@Composable
internal fun PhotosTab(
    groups: List<String>,
    selectedGroup: String,
    onSelected: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    val selectedIndex = groups.indexOf(selectedGroup)
    TabRow(
        selectedTabIndex = selectedIndex,
        containerColor = MaterialTheme.colorScheme.surface,
        indicator = { positions ->
            TabIndicatorContainer(tabPositions = positions, selectedIndex = selectedIndex) {
                val color = MaterialTheme.colorScheme.primary
                Canvas(modifier = Modifier.size(4.dp)) {
                    drawCircle(color)
                }
            }
        },
        modifier = modifier
    ) {
        groups.forEachIndexed { index, group ->
            val color = animateColorAsState(
                if (selectedGroup == group) MaterialTheme.colorScheme.primary else
                    MaterialTheme.colorScheme.onSurface.copy(alpha = 0.38f), label = "color_animation"
            )
            Tab(
                selected = index == selectedIndex,
                text = { Text(text = group, color = color.value) },
                onClick = {
                    onSelected(group)
                },
                selectedContentColor = MaterialTheme.colorScheme.surface
            )
        }
    }
}

@Composable
private fun TabIndicatorContainer(
    tabPositions: List<TabPosition>,
    selectedIndex: Int,
    content: @Composable() () -> Unit
) {
    val transition = updateTransition(targetState = selectedIndex, label = "selectedIndex")

    val offset = transition.animateDp(label = "tabPositions",
        transitionSpec = {
            spring(dampingRatio = Spring.DampingRatioMediumBouncy, stiffness = Spring.StiffnessLow)
        }) { idx ->
        val position = tabPositions[idx]
        (position.left + position.right) / 2
    }

    Box(
        modifier = Modifier
            .fillMaxSize()
            .wrapContentSize(Alignment.BottomStart)
            .offset { IntOffset(offset.value.roundToPx(), (-2).dp.roundToPx()) }
    ) {
        content()
    }
}
