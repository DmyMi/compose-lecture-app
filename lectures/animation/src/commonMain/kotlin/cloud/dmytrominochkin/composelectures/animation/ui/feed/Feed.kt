package cloud.dmytrominochkin.composelectures.animation.ui.feed

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.animation.model.User
import cloud.dmytrominochkin.composelectures.shared.components.dialog.SharedUsernameDialog
import cloud.dmytrominochkin.composelectures.shared.components.feed.SharedFeedFab
import cloud.dmytrominochkin.composelectures.shared.components.feed.SharedFeedItem
import kotlinx.coroutines.launch
import org.jetbrains.compose.resources.painterResource


@Composable
internal fun Feed(
    users: List<User>,
    onSelected: (Int) -> Unit,
    modifier: Modifier = Modifier
) {
    var isDialogVisible by remember {
        mutableStateOf(false)
    }
    var name by rememberSaveable {
        mutableStateOf("")
    }

    val snackbarHostState = remember { SnackbarHostState() }
    val coroutineScope = rememberCoroutineScope()

    Surface(modifier = modifier.fillMaxSize()) {
        val state = rememberLazyListState()
        LazyColumn(state = state) {
            item {
                FeedHeader(name)
            }
            items(users) { user ->
                SharedFeedItem(
                    userName = user.name,
                    lastOnline = user.lastOnline,
                    onClick = {
                        onSelected(user.id)
                    },
                    avatar = {
                        Image(
                            painter = painterResource(resource = user.avatar),
                            contentDescription = "Avatar",
                            modifier = Modifier
                                .size(48.dp)
                                .clip(CircleShape),
                            contentScale = ContentScale.Crop
                        )
                    },
                    cover = {
                        Image(
                            painter = painterResource(resource = user.coverImage),
                            contentDescription = "cover",
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(250.dp)
                                .shadow(elevation = 4.dp, clip = true, shape = RoundedCornerShape(4.dp)),
                            contentScale = ContentScale.Crop
                        )
                    }
                )
            }
        }
        SharedFeedFab(
            state = state,
            onClick = { isDialogVisible = true },
            modifier = Modifier.wrapContentSize(align = Alignment.BottomEnd)
        )
        if (isDialogVisible) {
            SharedUsernameDialog(
                name = name,
                onNameChange = { newName ->
                    name = newName
                },
                onDismiss = {
                    name = ""
                    isDialogVisible = false
                },
                onConfirm = {
                    coroutineScope.launch {
                        snackbarHostState.showSnackbar(message = "You are logged in as $name", duration = SnackbarDuration.Short)
                    }
                    isDialogVisible = false
                }
            )
        }
    }
}

