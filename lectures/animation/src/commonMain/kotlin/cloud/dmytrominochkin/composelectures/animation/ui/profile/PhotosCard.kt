package cloud.dmytrominochkin.composelectures.animation.ui.profile

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.ViewSwitch
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedPhotosGrid
import cloud.dmytrominochkin.composelectures.shared.rememberViewSwitchState
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.photos
import org.jetbrains.compose.resources.DrawableResource
import org.jetbrains.compose.resources.painterResource
import org.jetbrains.compose.resources.stringResource

@Composable
internal fun PhotosCard(
    groupedImages: Map<String, List<DrawableResource>>,
    modifier: Modifier = Modifier
) {
    val groups = groupedImages.keys.toList()
    //    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    val checkedState = rememberViewSwitchState()
    Surface(
        modifier = modifier.fillMaxWidth(),
        shadowElevation = 2.dp,
        shape = RoundedCornerShape(10, 10, 0, 0)
    ) {
        Column {
            var selectedGroup by rememberSaveable {
                mutableStateOf(groups.first())
            }
            Text(
                text = stringResource(Res.string.photos),
                style = MaterialTheme.typography.headlineSmall,
                modifier = Modifier.padding(
                    start = mediumPadding,
                    end = mediumPadding,
                    top = mediumPadding
                )
            )
            ViewSwitch(checkedState, "Lazy", "Custom")
            PhotosTab(
                groups = groups,
                selectedGroup = selectedGroup,
                onSelected = { grp -> selectedGroup = grp }
            )
            AnimatedContent(
                targetState = checkedState.isChecked,
                label = "grid",
                transitionSpec = {
                    fadeIn(tween(1000)) togetherWith fadeOut(tween(500))
                }
            ) {
                if (it) {
                    DynamicPhotosGrid(
                        images = groupedImages.getValue(selectedGroup),
                        modifier = Modifier
                            .padding(
                                top = mediumPadding / 2,
                                start = mediumPadding,
                                end = mediumPadding,
                                bottom = mediumPadding
                            )
                    )
                } else {
                    SharedPhotosGrid(
                        images = groupedImages.getValue(selectedGroup),
                        modifier = Modifier
                            .padding(
                                top = mediumPadding / 2,
                                start = mediumPadding,
                                end = mediumPadding,
                                bottom = mediumPadding
                            )
                    ) { image ->
                        Image(
                            painterResource(resource = image),
                            modifier = Modifier
                                .aspectRatio(1f)
                                .clip(RoundedCornerShape(16.dp)),
                            contentDescription = "photo",
                            contentScale = ContentScale.Crop
                        )
                    }
                }
            }
        }
    }
}
