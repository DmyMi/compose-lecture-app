package cloud.dmytrominochkin.composelectures.animation.ui.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.Placeable
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp
import org.jetbrains.compose.resources.DrawableResource
import org.jetbrains.compose.resources.painterResource

@Composable
fun DynamicPhotosGrid(
    images: List<DrawableResource>,
    modifier: Modifier = Modifier,
    verticalScrollState: ScrollState = rememberScrollState()
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    ) {
        Layout(
            content = {
                images.forEach {
                    Image(
                        painter = painterResource(resource = it),
                        modifier = Modifier
                            .aspectRatio(1f)
                            .clip(RoundedCornerShape(16.dp)),
                        contentDescription = "photo",
                        contentScale = ContentScale.Crop
                    )
                }
            },
            modifier = Modifier.verticalScroll(verticalScrollState)
        ) { measurables, constraints ->
            val padding = 8.dp.roundToPx()
            val maxWidth = constraints.maxWidth
            val cellSize = ((maxWidth - padding * 2) / 3).coerceAtLeast(0)

            if (measurables.isEmpty()) {
                layout(constraints.maxWidth, 0) {}
            } else if (measurables.size < 3) {
                // Less than 3 images: all images take normal size
                val smallImageConstraints = Constraints.fixedWidth((maxWidth - padding) / 2)
                val placeables = measurables.map { measurable ->
                    measurable.measure(smallImageConstraints)
                }

                val totalHeight = placeables.maxOf { it.height }
                layout(constraints.maxWidth, totalHeight) {
                    var xPos = 0
                    for (placeable in placeables) {
                        placeable.place(xPos, 0)
                        xPos += placeable.width + padding
                    }
                }
            } else {
                // 3 or more images: first image is larger (2x2 cells)
                val placeables = mutableListOf<Placeable>()

                // Measure the first image (big image)
                val bigImageConstraints = Constraints.fixed(
                    width = cellSize * 2 + padding,
                    height = cellSize * 2 + padding
                )
                val bigImagePlaceable = measurables[0].measure(bigImageConstraints)
                placeables.add(bigImagePlaceable)

                // Measure the remaining images (small images)
                val smallImageConstraints = Constraints.fixed(cellSize, cellSize)
                for (i in 1 until measurables.size) {
                    val placeable = measurables[i].measure(smallImageConstraints)
                    placeables.add(placeable)
                }

                val numSmallImages = placeables.size - 1

                // Determine layout dimensions
                val smallImagesPerRow = 3
                val imagesToRight = minOf(numSmallImages, 2)
                val imagesBelow = numSmallImages - imagesToRight
                val rowsBelow = (imagesBelow + smallImagesPerRow - 1) / smallImagesPerRow
                val totalHeight = bigImagePlaceable.height + padding + rowsBelow * (cellSize + padding)

                layout(constraints.maxWidth, totalHeight) {
                    var index = 0 // Index in placeables list

                    // Place the big image at the top-left corner
                    bigImagePlaceable.place(0, 0)
                    index++

                    // Place up to two small images to the right of the big image
                    var xPos = bigImagePlaceable.width + padding
                    var yPos = 0
                    for (i in 0 until imagesToRight) {
                        if (index >= placeables.size) break
                        placeables[index].place(xPos, yPos)
                        yPos += cellSize + padding
                        index++
                    }

                    // Place the remaining small images in rows below
                    yPos = bigImagePlaceable.height + padding
                    while (index < placeables.size) {
                        xPos = 0
                        for (i in 0 until smallImagesPerRow) {
                            if (index >= placeables.size) break
                            placeables[index].place(xPos, yPos)
                            xPos += cellSize + padding
                            index++
                        }
                        yPos += cellSize + padding
                    }
                }
            }
        }
        VerticalScrollbar(verticalScrollState)
    }
}

@Composable
internal expect fun BoxScope.VerticalScrollbar(scrollState: ScrollState)
