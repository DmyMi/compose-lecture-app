package cloud.dmytrominochkin.composelectures.animation

import androidx.compose.runtime.Composable

@Composable
internal expect fun BackHandler(isEnabled: Boolean = true, onBack: () -> Unit)