package cloud.dmytrominochkin.composelectures.animation.ui.profile

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.slideIn
import androidx.compose.animation.slideOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.unit.IntOffset
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.follow
import org.jetbrains.compose.resources.stringResource

@Composable
internal fun AnimatedFollowButton(
    onFollow: () -> Unit = { }
) {
    var flipper by remember {
        mutableStateOf(false)
    }

    AnimatedContent(
        targetState = flipper,
        label = "follow",
        transitionSpec = {
            slideIn { IntOffset(it.width / 2, it.height / 2) } togetherWith
                    slideOut { IntOffset(-it.width * 2, -it.height * 2) } using
                    SizeTransform(clip = false)
        }) { _ ->
        Button(
            onClick = {
                flipper = flipper.not()
                onFollow()
            }, shape = CircleShape
        ) {
            Text(text = stringResource(Res.string.follow))
        }
    }
}
