package cloud.dmytrominochkin.composelectures.animation.ui.profile

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.animation.model.User
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedTags


@Composable
internal fun Profile(
    user: User,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Surface(
        modifier = modifier,
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onSurface
    ) {
        Column(
//            Modifier.padding(top = dimensionResource(id = R.dimen.padding_large))
            Modifier.padding(top = 24.dp)
        ) {
            ProfileHeader(user = user)
            SharedTags(
                tags = user.tags,
                Modifier.padding(top = mediumPadding, bottom = mediumPadding))
            PhotosCard(groupedImages = user.photos)
        }
    }
}
