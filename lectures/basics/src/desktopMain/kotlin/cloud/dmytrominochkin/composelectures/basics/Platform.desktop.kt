package cloud.dmytrominochkin.composelectures.basics

/**
 * Actual implementation of [getPlatform] for the Desktop platform.
 *
 * @return [Platform.Desktop], indicating the code is running on Desktop.
 */

actual fun getPlatform(): Platform = Platform.Desktop