package cloud.dmytrominochkin.composelectures.basics

/**
 * Actual implementation of [getPlatform] for the Android platform.
 *
 * @return [Platform.Android], indicating the code is running on Android.
 */
actual fun getPlatform(): Platform = Platform.Android