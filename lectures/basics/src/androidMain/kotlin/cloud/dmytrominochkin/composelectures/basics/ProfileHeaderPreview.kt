package cloud.dmytrominochkin.composelectures.basics

import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import cloud.dmytrominochkin.composelectures.basics.model.User
import cloud.dmytrominochkin.composelectures.basics.ui.profile.ProfileHeader

/**
 * A preview of the [ProfileHeader] composable function, displayed within a [Surface].
 *
 * This function is annotated with `@Preview`, allowing it to be rendered in the IDE's preview pane.
 *
 * [Preview]:
 * - An annotation that enables design-time previews of composable functions in supported IDEs like Android Studio.
 * - Helps developers visualize the UI without running the app on a device or emulator.
 *
 * [Surface]:
 * - A composable that provides a Material Design surface, which can have a background color, elevation, and shape.
 * - Acts as a container that applies styling and theming to its child composables.
 *
 * @see ProfileHeader
 * @see Surface
 */
@Preview
@Composable
internal fun ProfileHeaderPreview() {
    Surface {
        ProfileHeader(user = User.example)
    }
}