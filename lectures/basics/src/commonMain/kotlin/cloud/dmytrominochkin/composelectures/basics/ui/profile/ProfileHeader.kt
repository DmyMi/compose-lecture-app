package cloud.dmytrominochkin.composelectures.basics.ui.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.basics.model.User
import org.jetbrains.compose.resources.painterResource

/**
 * Displays a user's profile header, including avatar, name, follower counts, and a follow button.
 *
 * This is a [Composable] function used in Jetpack Compose to build UI components.
 *
 * [Image]:
 * - A composable that displays an image on the screen.
 * - Can load images from resources, assets, or external sources.
 *
 * [Row]:
 * - A layout composable that places its children in a horizontal sequence.
 * - Supports alignment and spacing between its children.
 *
 * [Spacer]:
 * - A composable that creates an empty space with specified dimensions.
 * - Useful for adding spacing between other composables.
 *
 * [painterResource]:
 * - A function that loads an image resource and returns a [androidx.compose.ui.graphics.painter.Painter] object.
 * - Used with [Image] to display drawable resources.
 *
 * [org.jetbrains.compose.resources.DrawableResource]:
 * - Represents a drawable resource, such as an image or vector graphic.
 * - Can be used with [painterResource] to load the drawable.
 *
 * [CircleShape]:
 * - A shape that clips content to a circular form.
 * - Used with [Modifier.clip] to apply the shape to composables.
 *
 * @param user The [User] whose profile information is displayed.
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 */
@Composable
internal fun ProfileHeader(
    user: User,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp)
    ) {
        Image(
            painter = painterResource(resource = user.avatar),
            contentDescription = "Avatar",
            modifier = Modifier
                .size(72.dp)
                .clip(CircleShape)
        )
        Spacer(modifier = Modifier.size(16.dp))
        Column {
            Text(text = user.name)
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                FollowersInfo(number = user.numOfFollowers, text = "followers")
                FollowersInfo(number = user.numOfFollowing, text = "following")
                Text(text = "Follow", modifier = Modifier.border(1.dp, Color.Red))
            }
        }
    }
}
