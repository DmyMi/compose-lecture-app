package cloud.dmytrominochkin.composelectures.basics

/**
 * Defines the supported platforms.
 * This enum will be used to identify the platform the code is running on.
 */
enum class Platform {
    Android,
    Desktop
}

/**
 * Retrieves the current platform.
 *
 * The 'expect' keyword indicates that this function must be implemented in each platform module.
 * The actual implementations of this function will be provided in the platform-specific source sets.
 *
 * @return The current [Platform].
 */
expect fun getPlatform(): Platform