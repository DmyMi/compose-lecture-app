package cloud.dmytrominochkin.composelectures.basics

/**
 * Provides greeting messages based on the current platform.
 */
class Greeting {
    private val platform = getPlatform()

    /**
     * Generates a greeting message for the current platform.
     *
     * @return A greeting string that includes the platform name.
     */
    fun greet(): String {
        return "Hello, ${platform.name}!"
    }
}