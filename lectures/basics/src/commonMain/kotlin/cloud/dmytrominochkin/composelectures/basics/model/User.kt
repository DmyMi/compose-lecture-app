package cloud.dmytrominochkin.composelectures.basics.model

import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.avatar_1
import org.jetbrains.compose.resources.DrawableResource

/**
 * Represents a user profile with associated information.
 */
internal data class User(
    val id: Int,
    val name: String,
    val avatar: DrawableResource,
    val numOfFollowers: Int,
    val numOfFollowing: Int
) {
    companion object {
        val example = User(
            1,
            "Mary Jane",
            Res.drawable.avatar_1,
            1000,
            500
        )
    }
}