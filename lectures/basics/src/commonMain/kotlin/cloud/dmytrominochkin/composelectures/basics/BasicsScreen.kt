package cloud.dmytrominochkin.composelectures.basics

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.basics.model.User
import cloud.dmytrominochkin.composelectures.basics.ui.profile.ProfileHeader

/**
 * Displays the basic screen containing a profile header and a greeting message.
 *
 * This is a [Composable] function in Jetpack Compose used to build UI components declaratively.
 *
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 * @see Greeting
 * @see ProfileHeader
 */
@Composable
fun BasicsScreen(modifier: Modifier = Modifier) {
    Column(modifier = modifier) {
        ProfileHeader(user = User.example)
        Text(text = Greeting().greet())
    }
}