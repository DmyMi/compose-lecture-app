package cloud.dmytrominochkin.composelectures.basics.ui.profile

import androidx.compose.foundation.layout.Column
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

/**
 * Displays follower information in a vertical column layout.
 *
 * This is a [Composable] function, meaning it can be used within other composable functions to build
 * the UI in Jetpack Compose.
 * They allow you to describe your UI in terms of data and state, and the framework takes care of updating the UI when the data changes.
 *
 * [Column]:
 * - A layout composable that places its children in a vertical sequence.
 * - Supports alignment and arrangement of its children.
 *
 * [Text]:
 * - A composable that displays text content on the screen.
 * - Can be styled with different fonts, sizes, colors, and more.
 *
 * [Modifier]:
 * - Used to modify the appearance or layout of composables.
 * - Can be used to adjust properties like size, padding, alignment, and more.
 *
 * [Alignment.CenterHorizontally]:
 * - An alignment option that centers content horizontally within a container.
 *
 * @param number The numerical value to display, typically representing the count of followers.
 * @param text The descriptive text to display below the number.
 * @param modifier An optional [Modifier] to adjust the layout or appearance. Defaults to [Modifier].
 */
@Composable
internal fun FollowersInfo(number: Int, text: String, modifier: Modifier = Modifier) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        Text(text = number.toString())
        Text(text = text)
    }
}