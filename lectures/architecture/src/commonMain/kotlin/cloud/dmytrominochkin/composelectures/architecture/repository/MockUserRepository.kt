package cloud.dmytrominochkin.composelectures.architecture.repository

import cloud.dmytrominochkin.composelectures.architecture.datasource.DataSource
import cloud.dmytrominochkin.composelectures.architecture.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

internal class MockUserRepository(
    private val dataSource: DataSource
) : UserRepository {
    override suspend fun getUser(id: Int): Result<User> =
        withContext(Dispatchers.IO) {
            Result.success(dataSource.getUser(id))
        }

    override suspend fun loadFeed(): Result<List<User>> =
        withContext(Dispatchers.IO) {
            try {
                Result.success(dataSource.loadFeed())
            } catch (e: IllegalStateException) {
                Result.failure(e)
            }
        }
}
