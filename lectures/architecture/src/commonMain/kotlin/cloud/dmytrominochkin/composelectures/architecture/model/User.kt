package cloud.dmytrominochkin.composelectures.architecture.model

import org.jetbrains.compose.resources.DrawableResource

internal data class User(
    val id: Int,
    val name: String,
    val avatar: DrawableResource,
    val numOfFollowers: Int,
    val numOfFollowing: Int,
    val lastOnline: String,
    val coverImage: DrawableResource,
    val tags: List<String>,
    val photos: Map<String, List<DrawableResource>>,
)
