package cloud.dmytrominochkin.composelectures.architecture.ui.feed

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import cloud.dmytrominochkin.composelectures.architecture.datasource.MockNetDataSource
import cloud.dmytrominochkin.composelectures.architecture.model.User
import cloud.dmytrominochkin.composelectures.architecture.repository.MockUserRepository
import cloud.dmytrominochkin.composelectures.architecture.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch

internal class FeedViewModel() : ViewModel() {
    private val _uiState = MutableStateFlow(FeedState())
    val uiState = _uiState.asStateFlow()

    val userRepository: UserRepository = MockUserRepository(MockNetDataSource())

    fun loadFeed() {
        _uiState.update { oldState -> oldState.copy(isLoading = true, isError = false) }
        viewModelScope.launch {
            userRepository.loadFeed()
                .onFailure {
                    _uiState.update { oldState -> oldState.copy(isLoading = false, isError = true, users = emptyList()) }
                }
                .onSuccess {
                    _uiState.update { oldState -> oldState.copy(isLoading = false, isError = false, users = it) }
                }
        }
    }

    fun updateName(name: String) {
        _uiState.update { oldState -> oldState.copy(name = name) }
    }

    fun showDialog() {
        _uiState.update { oldState -> oldState.copy(isDialogVisible = true) }
    }

    fun hideDialog() {
        _uiState.update { oldState -> oldState.copy(isDialogVisible = false) }
    }
}

internal data class FeedState(
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val name: String = "",
    val isDialogVisible: Boolean = false,
    val users: List<User> = emptyList()
)
