package cloud.dmytrominochkin.composelectures.architecture

import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.architecture.ui.feed.Feed
import cloud.dmytrominochkin.composelectures.architecture.ui.profile.Profile
import cloud.dmytrominochkin.composelectures.shared.components.SharedBackHandler
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.profile
import org.jetbrains.compose.resources.stringResource


/** In case of this simple state navigation,
 * the ViewModels that are added here are going to be scoped to the Activity.
 * It mean they are effectively singleton for the Compose application which is not optimal/good
 * in most cases.
 * So for this type of application we can use Compose state holder pattern instead of ViewModels.
 * But it is not covered in lectures.
 */
@Composable
internal fun MainScreenCross(
    modifier: Modifier = Modifier
) {
    var selectedId by rememberSaveable {
        mutableStateOf<Int?>(null)
    }
    val snackbarHostState = remember { SnackbarHostState() }
    Scaffold(
        modifier = modifier,
        topBar = {
            if (selectedId != null) {
                AppBarCross(navigateUp = { selectedId = null })
            }
        },
        snackbarHost = {
            SnackbarHost(hostState = snackbarHostState)
        }
    ) { innerPadding ->
        Crossfade(
            targetState = selectedId,
            label = "crossfade",
            modifier = Modifier.padding(innerPadding)
        ) { id ->
            if (id == null) {
                Feed(SnackbarHostState(), onSelected = { userId ->
                    selectedId = userId
                })
            } else {
                Profile(userId = id)
                SharedBackHandler {
                    selectedId = null
                }
            }
        }
    }
}
@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun AppBarCross(
    navigateUp: () -> Unit,
    modifier: Modifier = Modifier
) {
    TopAppBar(
        title = { Text(stringResource(Res.string.profile)) },
        colors = TopAppBarDefaults.mediumTopAppBarColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        ),
        modifier = modifier,
        navigationIcon = {
            IconButton(onClick = navigateUp) {
                Icon(imageVector = Icons.Filled.ArrowBack, contentDescription = "back")
            }
        })
}
