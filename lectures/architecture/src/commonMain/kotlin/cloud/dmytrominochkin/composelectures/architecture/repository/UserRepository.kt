package cloud.dmytrominochkin.composelectures.architecture.repository

import cloud.dmytrominochkin.composelectures.architecture.model.User

internal interface UserRepository {
    suspend fun getUser(id: Int): Result<User>
    suspend fun loadFeed(): Result<List<User>>
}
