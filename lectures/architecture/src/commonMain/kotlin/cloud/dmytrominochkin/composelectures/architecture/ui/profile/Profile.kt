package cloud.dmytrominochkin.composelectures.architecture.ui.profile

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedPhotosCard
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedProfileHeader
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedTags
import org.jetbrains.compose.resources.painterResource

/**
 * On non-JVM platforms, objects cannot be instantiated using type reflection. So in common code you cannot call the viewModel() function without parameters: every time a ViewModel instance is created, you need to provide at least an initializer as an argument.
 *
 * If only an initializer is provided, the library creates a default factory under the hood. But you can implement your own factories and call more explicit versions of the common viewModel(...) function, just like with Jetpack Compose.
 */
@Composable
internal fun Profile(
    userId: Int,
    modifier: Modifier = Modifier,
    profileViewModel: ProfileViewModel = viewModel { ProfileViewModel() }
) {
    val uiState by profileViewModel.uiState.collectAsState()
    LaunchedEffect(true) {
        profileViewModel.getUser(userId)
    }
    Content(state = uiState, modifier = modifier.fillMaxSize())
}

@Composable
internal fun Content(
    state: ProfileState,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Surface(
        modifier = modifier,
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onSurface
    ) {
        AnimatedContent(
            targetState = state.showUser,
            label = "loading",
            transitionSpec = {
                scaleIn() togetherWith scaleOut() using SizeTransform(clip = true)
            }) { showUser ->
            if (showUser) {
                // We know user is not null here
                Column(
//            Modifier.padding(top = dimensionResource(id = R.dimen.padding_large))
                    Modifier.padding(top = 24.dp)
                ) {
                    SharedProfileHeader(
                        userName = state.user!!.name,
                        numOfFollowers = state.user.numOfFollowers,
                        numOfFollowing = state.user.numOfFollowing
                    ) {
                        Image(
                            painter = painterResource(resource = state.user.avatar),
                            contentDescription = "Avatar",
                            modifier = Modifier
                                .size(72.dp)
                                .clip(CircleShape),
                            contentScale = ContentScale.Crop
                        )
                    }
                    SharedTags(
                        tags = state.user.tags,
                        Modifier.padding(top = mediumPadding, bottom = mediumPadding))
                    SharedPhotosCard(groupedImages = state.user.photos) { image ->
                        Image(
                            painterResource(resource = image),
                            modifier = Modifier
                                .aspectRatio(1f)
                                .clip(RoundedCornerShape(16.dp)),
                            contentDescription = "photo",
                            contentScale = ContentScale.Crop
                        )
                    }
                }
            } else {
                Box(contentAlignment = Alignment.Center) {
                    CircularProgressIndicator(
                        modifier = Modifier.size(50.dp, 50.dp)
                    )
                }
            }
        }
    }
}

