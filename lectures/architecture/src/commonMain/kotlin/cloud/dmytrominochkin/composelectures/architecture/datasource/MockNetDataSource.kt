package cloud.dmytrominochkin.composelectures.architecture.datasource

import cloud.dmytrominochkin.composelectures.architecture.model.User
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.avatar_1
import cloud.dmytrominochkin.composelectures.shared.resources.avatar_2
import cloud.dmytrominochkin.composelectures.shared.resources.avatar_3
import cloud.dmytrominochkin.composelectures.shared.resources.image_1
import cloud.dmytrominochkin.composelectures.shared.resources.image_2
import cloud.dmytrominochkin.composelectures.shared.resources.image_3
import cloud.dmytrominochkin.composelectures.shared.resources.user1_1
import cloud.dmytrominochkin.composelectures.shared.resources.user1_2
import cloud.dmytrominochkin.composelectures.shared.resources.user1_3
import cloud.dmytrominochkin.composelectures.shared.resources.user1_4
import cloud.dmytrominochkin.composelectures.shared.resources.user1_5
import cloud.dmytrominochkin.composelectures.shared.resources.user1_6
import cloud.dmytrominochkin.composelectures.shared.resources.user2_1
import cloud.dmytrominochkin.composelectures.shared.resources.user2_2
import cloud.dmytrominochkin.composelectures.shared.resources.user2_3
import cloud.dmytrominochkin.composelectures.shared.resources.user2_4
import cloud.dmytrominochkin.composelectures.shared.resources.user2_5
import cloud.dmytrominochkin.composelectures.shared.resources.user2_6
import cloud.dmytrominochkin.composelectures.shared.resources.user3_1
import cloud.dmytrominochkin.composelectures.shared.resources.user3_2
import cloud.dmytrominochkin.composelectures.shared.resources.user3_3
import cloud.dmytrominochkin.composelectures.shared.resources.user3_4
import cloud.dmytrominochkin.composelectures.shared.resources.user3_5
import cloud.dmytrominochkin.composelectures.shared.resources.user3_6
import kotlinx.coroutines.delay
import kotlin.random.Random

internal class MockNetDataSource : DataSource {

    // Helper variable to simulate errors
    private var requestCount = 0

    // Helper function to fail every 5th request
    private fun isError(): Boolean = ++requestCount % 5 == 0

    override suspend fun getUser(id: Int): User {
        // Random delay to simulate internet :)
        delay(Random.nextLong(500, 1001))
        return users.first { it.id == id }
    }

    override suspend fun loadFeed(): List<User> {
        // Random delay to simulate internet :)
        delay(Random.nextLong(500, 2001))
        return if (isError()) {
            throw IllegalStateException("Some internet error")
        } else {
            users
        }
    }

    companion object {
        val users = mutableListOf(
            User(
                1,
                "Mary Jane",
                Res.drawable.avatar_1,
                1000,
                500,
                "3 minutes ago",
                Res.drawable.image_1,
                listOf(
                    "food",
                    "fashion",
                    "nature",
                    "dogs",
                    "people",
                    "selfies",
                    "style",
                    "fashion",
                    "cats"
                ),
                mapOf(
                    "Today" to listOf(
                        Res.drawable.user1_1,
                        Res.drawable.user1_2,
                        Res.drawable.user1_3,
                        Res.drawable.user1_4,
                        Res.drawable.user1_5,
                        Res.drawable.user1_6
                    ),
                    "Hobby" to listOf(
                        Res.drawable.user2_1,
                        Res.drawable.user2_2,
                        Res.drawable.user2_3,
                        Res.drawable.user2_4,
                        Res.drawable.user2_5,
                        Res.drawable.user2_6
                    )
                )
            ),
            User(
                2,
                "Hugh Jass",
                Res.drawable.avatar_2,
                2000,
                999,
                "10 minutes ago",
                Res.drawable.image_2,
                listOf("people", "selfies", "style", "fashion"),
                mapOf(
                    "Vacation" to listOf(
                        Res.drawable.user1_1,
                        Res.drawable.user1_2,
                        Res.drawable.user1_3,
                        Res.drawable.user1_4,
                        Res.drawable.user1_5,
                        Res.drawable.user1_6
                    ),
                    "Today" to listOf(
                        Res.drawable.user2_1,
                        Res.drawable.user2_2,
                        Res.drawable.user2_3,
                        Res.drawable.user2_4,
                        Res.drawable.user2_5,
                        Res.drawable.user2_6
                    ),
                    "Inspiration" to listOf(
                        Res.drawable.user3_1,
                        Res.drawable.user3_2,
                        Res.drawable.user3_3,
                        Res.drawable.user3_4,
                        Res.drawable.user3_5,
                        Res.drawable.user3_6
                    )
                )
            ),
            User(
                3,
                "Tess Tickle",
                Res.drawable.avatar_3,
                69,
                420,
                "1 day ago",
                Res.drawable.image_3,
                listOf("selfie", "cats", "nature", "fashion"),
                mapOf(
                    "Life" to listOf(
                        Res.drawable.user3_1,
                        Res.drawable.user3_2,
                        Res.drawable.user3_3,
                        Res.drawable.user3_4,
                        Res.drawable.user3_5,
                        Res.drawable.user3_6
                    ),
                    "Hobby" to listOf(
                        Res.drawable.user1_1,
                        Res.drawable.user1_2,
                        Res.drawable.user1_3,
                        Res.drawable.user1_4,
                        Res.drawable.user1_5,
                        Res.drawable.user1_6
                    )
                )
            )
        )
    }
}
