package cloud.dmytrominochkin.composelectures.architecture.datasource

import cloud.dmytrominochkin.composelectures.architecture.model.User

internal interface DataSource {
    suspend fun getUser(id: Int): User
    suspend fun loadFeed(): List<User>
}
