package cloud.dmytrominochkin.composelectures.architecture

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.shared.ViewSwitch
import cloud.dmytrominochkin.composelectures.shared.rememberViewSwitchState

@Composable
fun ArchitectureScreen(modifier: Modifier = Modifier) {
    val checkedState = rememberViewSwitchState()
    Column(modifier = modifier) {
        ViewSwitch(checkedState, "Crossfade", "Navigation")
        if (checkedState.isChecked) {
            MainScreenNav(modifier)
        } else {
            MainScreenCross(modifier)
        }
    }
}