package cloud.dmytrominochkin.composelectures.architecture

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.architecture.datasource.MockNetDataSource
import cloud.dmytrominochkin.composelectures.architecture.ui.feed.Content
import cloud.dmytrominochkin.composelectures.architecture.ui.feed.FeedState
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun FeedPreview() {
    ComposeLectureTheme {
        Content(SnackbarHostState(), FeedState(users = MockNetDataSource.users), {}, {}, {}, {}, {})
    }
}
