package cloud.dmytrominochkin.composelectures.internet.di

import cloud.dmytrominochkin.composelectures.internet.api.NetworkDataSource
import cloud.dmytrominochkin.composelectures.internet.repository.UserRepository
import cloud.dmytrominochkin.composelectures.internet.repository.UserRepositoryImpl
import cloud.dmytrominochkin.composelectures.internet.ui.feed.FeedViewModel
import cloud.dmytrominochkin.composelectures.internet.ui.profile.ProfileViewModel
import io.ktor.client.HttpClient
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import org.koin.core.module.dsl.singleOf
import org.koin.core.module.dsl.viewModelOf
import org.koin.dsl.bind
import org.koin.dsl.module

internal val appModule = module {

    single {
        HttpClient(OkHttp) {
            install(ContentNegotiation) {
                json(Json {
                    ignoreUnknownKeys = true
                })
            }
        }
    }
    singleOf(::NetworkDataSource)
    singleOf(::UserRepositoryImpl) bind UserRepository::class

    viewModelOf(::FeedViewModel)
    viewModelOf(::ProfileViewModel)
}

fun internetModules() = listOf(appModule)
