package cloud.dmytrominochkin.composelectures.internet.ui.feed

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.SnackbarDuration
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.SnackbarResult
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.internet.BASE_URL
import cloud.dmytrominochkin.composelectures.shared.components.dialog.SharedUsernameDialog
import cloud.dmytrominochkin.composelectures.shared.components.feed.SharedFeedFab
import cloud.dmytrominochkin.composelectures.shared.components.feed.SharedFeedHeader
import cloud.dmytrominochkin.composelectures.shared.components.feed.SharedFeedItem
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.placeholder
import coil3.compose.AsyncImage
import kotlinx.coroutines.launch
import org.jetbrains.compose.resources.painterResource
import org.koin.compose.viewmodel.koinViewModel


@Composable
internal fun Feed(
    snackbarHostState: SnackbarHostState,
    onSelected: (Int) -> Unit,
    modifier: Modifier = Modifier,
    feedViewModel: FeedViewModel = koinViewModel()
) {
    val uiState by feedViewModel.uiState.collectAsState()

    LaunchedEffect(true) {
        if (uiState.users.isEmpty()) feedViewModel.loadFeed()
    }

    Content(
        snackbarHostState,
        uiState,
        onSelected,
        feedViewModel::loadFeed,
        feedViewModel::showDialog,
        feedViewModel::hideDialog,
        feedViewModel::updateName,
        modifier
    )
}

@Composable
internal fun Content(
    snackbarHostState: SnackbarHostState,
    uiState: FeedState,
    onSelected: (Int) -> Unit,
    loadFeed: () -> Unit,
    showDialog: () -> Unit,
    hideDialog: () -> Unit,
    updateName: (String) -> Unit,
    modifier: Modifier = Modifier,
) {
    val coroutineScope = rememberCoroutineScope()

    if (uiState.isError) {
        LaunchedEffect(snackbarHostState) {
            val result = snackbarHostState.showSnackbar(
                message = "Sorry, there is network error!",
                actionLabel = "Reload",
                duration = SnackbarDuration.Long
            )
            when (result) {
                SnackbarResult.ActionPerformed -> { loadFeed() }
                SnackbarResult.Dismissed -> {}
            }
        }
    }

    Surface(modifier = modifier.fillMaxSize()) {
        val state = rememberLazyListState()
        LazyColumn(state = state) {
            item {
                SharedFeedHeader(onIconClick = loadFeed, uiState.name)
            }
            if (uiState.isLoading) {
                item {
                    LinearProgressIndicator(
                        color = MaterialTheme.colorScheme.primary,
                        modifier = Modifier.fillMaxWidth()
                    )
                }
            }
            items(uiState.users) { user ->
                SharedFeedItem(
                    userName = user.name,
                    lastOnline = user.lastOnline,
                    onClick = {
                        onSelected(user.id)
                    },
                    avatar = {
                        AsyncImage(
                            model = "${BASE_URL}${user.avatar}",
                            placeholder = painterResource(resource = Res.drawable.placeholder),
                            contentDescription = "Avatar",
                            modifier = Modifier
                                .size(48.dp)
                                .clip(CircleShape),
                            contentScale = ContentScale.Crop
                        )
                    },
                    cover = {
                        AsyncImage(
                            model = "$BASE_URL${user.coverImage}",
                            placeholder = painterResource(resource = Res.drawable.placeholder),
                            contentDescription = "cover",
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(250.dp)
                                .shadow(elevation = 4.dp, clip = true, shape = RoundedCornerShape(4.dp)),
                            contentScale = ContentScale.Crop
                        )
                    }
                )
            }
        }
        SharedFeedFab(
            state = state,
            onClick = showDialog,
            modifier = Modifier.wrapContentSize(align = Alignment.BottomEnd)
        )
        if (uiState.isDialogVisible) {
            SharedUsernameDialog(
                name = uiState.name,
                onNameChange = updateName,
                onDismiss = {
                    updateName("")
                    hideDialog()
                },
                onConfirm = {
                    coroutineScope.launch {
                        snackbarHostState.showSnackbar(message = "You are logged in as ${uiState.name}", duration = SnackbarDuration.Short)
                    }
                    hideDialog()
                }
            )
        }
    }
}

