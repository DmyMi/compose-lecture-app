package cloud.dmytrominochkin.composelectures.internet.data

internal sealed class DataState<out R> {
    class Loading<T>(
        val cachedData: T? = null
    ) : DataState<T>()

    class Success<T>(
        val data: T
    ) : DataState<T>()

    class Error(
        val error: Throwable
    ) : DataState<Nothing>()
}

internal val <T> DataState<T>.data: T?
    get() = (this as? DataState.Success)?.data ?: (this as? DataState.Loading)?.cachedData