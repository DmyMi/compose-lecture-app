package cloud.dmytrominochkin.composelectures.internet.ui.profile

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.SizeTransform
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.internet.BASE_URL
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedPhotosCard
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedProfileHeader
import cloud.dmytrominochkin.composelectures.shared.components.profile.SharedTags
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.placeholder
import coil3.compose.AsyncImage
import org.jetbrains.compose.resources.painterResource
import org.koin.compose.viewmodel.koinViewModel


@Composable
internal fun Profile(
    userId: Int,
    modifier: Modifier = Modifier,
    profileViewModel: ProfileViewModel = koinViewModel()
) {
    val uiState by profileViewModel.uiState.collectAsState()
    LaunchedEffect(true) {
        profileViewModel.getUser(userId)
    }
    Content(
        state = uiState,
        modifier = modifier.fillMaxSize()
    )
}

@Composable
internal fun Content(
    state: ProfileState,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = 16.dp
    Surface(
        modifier = modifier,
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onSurface
    ) {
        AnimatedContent(
            targetState = state.showUser,
            label = "loading",
            transitionSpec = {
                scaleIn() togetherWith scaleOut() using SizeTransform(clip = true)
            }) { showUser ->
            if (showUser) {
                // We know user is not null here
                Column(
//            Modifier.padding(top = dimensionResource(id = R.dimen.padding_large))
                    Modifier.padding(top = 24.dp)
                ) {
                    SharedProfileHeader(
                        userName = state.user!!.name,
                        numOfFollowers = state.user.numOfFollowers,
                        numOfFollowing = state.user.numOfFollowing
                    ) {
                        AsyncImage(
                            model = "$BASE_URL${state.user.avatar}",
                            placeholder = painterResource(resource = Res.drawable.placeholder),
                            contentDescription = "Avatar",
                            modifier = Modifier
                                .size(72.dp)
                                .clip(CircleShape),
                            contentScale = ContentScale.Crop
                        )
                    }
                    if (state.user.tags.isEmpty() or state.user.photos.isEmpty()) {
                        LinearProgressIndicator(
                            color = MaterialTheme.colorScheme.primary,
                            modifier = Modifier.fillMaxWidth()
                        )
                    }
                    if (state.user.tags.isNotEmpty()) {
                        SharedTags(
                            tags = state.user.tags,
                            Modifier.padding(top = mediumPadding, bottom = mediumPadding)
                        )
                    }
                    if (state.user.photos.isNotEmpty()) {
                        SharedPhotosCard(groupedImages = state.user.photos) { image ->
                            AsyncImage(
                                model = "$BASE_URL$image",
                                placeholder = painterResource(resource = Res.drawable.placeholder),
                                modifier = Modifier
                                    .aspectRatio(1f)
                                    .clip(RoundedCornerShape(16.dp)),
                                contentDescription = "photo",
                                contentScale = ContentScale.Crop
                            )
                        }
                    }
                }
            } else {
                Box(contentAlignment = Alignment.Center) {
                    CircularProgressIndicator(
                        modifier = Modifier.size(50.dp, 50.dp)
                    )
                }
            }
        }
    }
}

