package cloud.dmytrominochkin.composelectures.internet.repository

import cloud.dmytrominochkin.composelectures.internet.api.ApiFeedUser
import cloud.dmytrominochkin.composelectures.internet.api.NetworkDataSource
import cloud.dmytrominochkin.composelectures.internet.data.DataState
import cloud.dmytrominochkin.composelectures.internet.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

internal class UserRepositoryImpl(
    private val api: NetworkDataSource,
) : UserRepository {
    override fun getUser(id: Int): Flow<DataState<User>> = flow {
        emit(DataState.Loading())
        try {
            val u = api.getUser(id)
            emit(DataState.Success(u.toUser()))
        } catch (t: Throwable) {
            emit(DataState.Error(t))
        }
    }
        .flowOn(Dispatchers.IO)

    override fun loadFeed(): Flow<DataState<List<User>>> = flow {
        emit(DataState.Loading())

        try {
            val response = api.loadFeed()
            emit(DataState.Success(response.map(ApiFeedUser::toUser)))
        } catch (t: Throwable) {
            emit(DataState.Error(t))
        }
    }
        .flowOn(Dispatchers.IO)
}
