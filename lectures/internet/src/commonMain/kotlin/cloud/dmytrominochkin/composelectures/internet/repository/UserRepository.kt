package cloud.dmytrominochkin.composelectures.internet.repository

import cloud.dmytrominochkin.composelectures.internet.data.DataState
import cloud.dmytrominochkin.composelectures.internet.model.User

import kotlinx.coroutines.flow.Flow

internal interface UserRepository {
    fun getUser(id: Int): Flow<DataState<User>>
    fun loadFeed(): Flow<DataState<List<User>>>
}
