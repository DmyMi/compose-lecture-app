package cloud.dmytrominochkin.composelectures.internet.model

internal data class User(
    val id: Int,
    val name: String,
    val avatar: String,
    val numOfFollowers: Int,
    val numOfFollowing: Int,
    val coverImage: String,
    val lastOnline: String,
    val tags: List<String>,
    val photos: Map<String, List<String>>,
)

