package cloud.dmytrominochkin.composelectures.internet.repository

import cloud.dmytrominochkin.composelectures.internet.api.ApiFeedUser
import cloud.dmytrominochkin.composelectures.internet.api.ApiUser
import cloud.dmytrominochkin.composelectures.internet.model.User

internal fun ApiFeedUser.toUser() = User(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline,
    tags = emptyList(),
    photos = emptyMap()
)

internal fun ApiUser.toUser() = User(
    id = id,
    name = name,
    avatar = avatar,
    numOfFollowers = numOfFollowers,
    numOfFollowing = numOfFollowing,
    coverImage = coverImage,
    lastOnline = lastOnline,
    tags = tags,
    photos = photos
)
