package cloud.dmytrominochkin.composelectures.internet

import androidx.compose.material3.SnackbarHostState
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.internet.data.users
import cloud.dmytrominochkin.composelectures.internet.ui.feed.Content
import cloud.dmytrominochkin.composelectures.internet.ui.feed.FeedState
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme

@TranslationPreview
@Composable
internal fun FeedPreview() {
    ComposeLectureTheme {
        Content(SnackbarHostState(), FeedState(users = users), {}, {}, {}, {}, {})
    }
}
