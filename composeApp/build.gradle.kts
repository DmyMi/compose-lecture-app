import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.ExperimentalKotlinGradlePluginApi
import org.jetbrains.kotlin.gradle.dsl.JvmTarget

plugins {
    alias(libs.plugins.kotlinMultiplatform)
    alias(libs.plugins.androidApplication)
    alias(libs.plugins.jetbrainsCompose)
    alias(libs.plugins.compose.compiler)
    id("com.github.triplet.play")
}

kotlin {
    androidTarget {
        @OptIn(ExperimentalKotlinGradlePluginApi::class)
        compilerOptions {
            jvmTarget.set(JvmTarget.JVM_17)
        }
    }

    jvm("desktop")

    sourceSets {
        val desktopMain by getting

        androidMain.dependencies {
            implementation(compose.preview)
            implementation(libs.androidx.activity.compose)
            implementation(libs.koin.compose.androidx)
        }

        commonMain.dependencies {
            implementation(projects.shared)
            implementation(projects.lectures.basics)
            implementation(projects.lectures.resources)
            implementation(projects.lectures.state)
            implementation(projects.lectures.animation)
            implementation(projects.lectures.navigation)
            implementation(projects.lectures.architecture)
            implementation(projects.lectures.injection)
            implementation(projects.lectures.internet)
            implementation(projects.lectures.room)
            implementation(compose.runtime)
            implementation(compose.foundation)
            implementation(compose.material3)
            implementation(compose.ui)
            implementation(compose.materialIconsExtended)
            implementation(compose.components.uiToolingPreview)
            implementation(libs.androidx.lifecycle.viewmodel)
            implementation(libs.androidx.lifecycle.runtime.compose)
            implementation(libs.navigation.compose)
            implementation(libs.koin.compose.core)
            implementation(libs.kottie)
        }

        desktopMain.dependencies {
            implementation(compose.desktop.currentOs)
            implementation(libs.kotlinx.coroutines.swing)
        }
    }
}

android {
    namespace = "cloud.dmytrominochkin.composelectures"
    compileSdk = libs.versions.android.compileSdk.get().toInt()

    defaultConfig {
        applicationId = "cloud.dmytrominochkin.composelectures"
        minSdk = libs.versions.android.minSdk.get().toInt()
        targetSdk = libs.versions.android.targetSdk.get().toInt()
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        setProperty("archivesBaseName", "$applicationId-v$versionName($versionCode)")
    }

    signingConfigs {
        getByName("debug") {
        }

        create("release") {
            storeFile = file(
                System.getenv("ANDROID_KEYSTORE_FILE")
                    ?: findProperty("ANDROID_KEYSTORE_FILE") as String
            )
            storePassword = System.getenv("ANDROID_KEYSTORE_PASSWORD")
                ?: findProperty("ANDROID_KEYSTORE_PASSWORD") as String
            keyAlias = System.getenv("ANDROID_KEYSTORE_ALIAS")
                ?: findProperty("ANDROID_KEYSTORE_ALIAS") as String
            keyPassword = System.getenv("ANDROID_KEYSTORE_PRIVATE_KEY_PASSWORD")
                ?: findProperty("ANDROID_KEYSTORE_PRIVATE_KEY_PASSWORD") as String
            enableV2Signing = true
        }
    }

    buildTypes {
        getByName("debug") {
            signingConfig = signingConfigs.getByName("debug")
        }
        release {
            isMinifyEnabled = false
            isDebuggable = false
            signingConfig = signingConfigs.getByName("release")
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    buildFeatures {
        compose = true
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }

    dependencies {
        debugImplementation(compose.uiTooling)
    }
}

compose.desktop {
    application {
        mainClass = "cloud.dmytrominochkin.composelectures.DesktopAppKt"

        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "Compose Lectures"
            packageVersion = "1.0.0"
        }
    }
}

play {
    serviceAccountCredentials.set(
        file(
            System.getenv("ANDROID_PUBLISHER_CREDENTIALS")
                ?: findProperty("ANDROID_PUBLISHER_CREDENTIALS") as String
        )
    )
    defaultToAppBundles.set(true)
    track.set("alpha")
    promoteTrack.set("beta")
    userFraction.set(1.0)
    updatePriority.set(5)
}