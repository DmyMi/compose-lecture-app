package cloud.dmytrominochkin.composelectures

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.NavigationRail
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationRailItem
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import cloud.dmytrominochkin.composelectures.animation.AnimationScreen
import cloud.dmytrominochkin.composelectures.architecture.ArchitectureScreen
import cloud.dmytrominochkin.composelectures.basics.BasicsScreen
import cloud.dmytrominochkin.composelectures.injection.InjectionScreen
import cloud.dmytrominochkin.composelectures.internet.InternetScreen
import cloud.dmytrominochkin.composelectures.internet.di.internetModules
import cloud.dmytrominochkin.composelectures.navigation.NavigationScreen
import cloud.dmytrominochkin.composelectures.resources.ResourcesScreen
import cloud.dmytrominochkin.composelectures.room.RoomScreen
import cloud.dmytrominochkin.composelectures.room.di.roomModules
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.under_construction
import cloud.dmytrominochkin.composelectures.shared.theme.ComposeLectureTheme
import cloud.dmytrominochkin.composelectures.state.StateScreen
import cloud.dmytrominochkin.composelectures.ui.components.UnderConstruction
import org.koin.compose.KoinIsolatedContext
import org.koin.dsl.koinApplication


fun main() = application {
    Window(
        onCloseRequest = ::exitApplication,
        title = "Compose Lectures"
    ) {
        ComposeLectureTheme {
            DesktopApp()
        }
    }
}

@Composable
fun DesktopApp(
    navController: NavHostController = rememberNavController()
) {
    Row {
        NavigationRail {
            MenuContent(
                menus,
                navController::navigate
            ) { idx, menu, (value, setValue), onMenuClick ->
                NavigationRailItem(
                    label = { Text(text = menu.title) },
                    icon = { Icon(imageVector = menu.icon, contentDescription = null) },
                    selected = value == idx,
                    onClick = {
                        setValue(idx)
                        onMenuClick(menu.route)
                    },
                    alwaysShowLabel = true
                )
            }
        }
        NavHost(navController = navController, startDestination = aboutRoute) {
            composable(aboutRoute) {
                Surface(modifier = Modifier) {
                    AboutScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(basicsRoute) {
                Surface(modifier = Modifier) {
                    BasicsScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(resourcesRoute) {
                Surface(modifier = Modifier) {
                    ResourcesScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(stateRoute) {
                Surface(modifier = Modifier) {
                    StateScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(animationRoute) {
                Surface(modifier = Modifier) {
                    AnimationScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(navigationRoute) {
                Surface(modifier = Modifier) {
                    NavigationScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(architectureRoute) {
                Surface(modifier = Modifier) {
                    ArchitectureScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(injectionRoute) {
                Surface(modifier = Modifier) {
                    InjectionScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            composable(internetRoute) {
                KoinIsolatedContext(context = koinApplication {
                    modules(internetModules())
                }) {
                    Surface(modifier = Modifier) {
                        InternetScreen(
                            modifier = Modifier
                                .fillMaxSize()
                        )
                    }
                }
            }
            composable(roomRoute) {
                KoinIsolatedContext(context = koinApplication {
                    modules(roomModules())
                }) {
                    Surface(modifier = Modifier) {
                        RoomScreen(
                            modifier = Modifier
                                .fillMaxSize()
                        )
                    }
                }
            }
            composable(extraRoute) {
                Surface(modifier = Modifier) {
                    UnderConstruction(
                        label = Res.string.under_construction,
                        path = "files/underconstruction.json",
                        modifier = Modifier.fillMaxSize()
                    )
                }
            }
        }
    }
}