package cloud.dmytrominochkin.composelectures

import android.app.Application
import cloud.dmytrominochkin.composelectures.injection.di.AppContainer
import cloud.dmytrominochkin.composelectures.injection.di.AppContainerProvider
import cloud.dmytrominochkin.composelectures.injection.di.AppDataContainer

class SocialApplication : Application(), AppContainerProvider {
    /**
     * [AppContainer] instance used by the rest of classes to obtain dependencies
     */
    override val appContainer: AppContainer by lazy {
        AppDataContainer()
    }

    override fun onCreate() {
        super.onCreate()
        // this is for a "pure" experiment in demo :)
        // no need to drop database everytime in real app
//        deleteDatabase("user_database")
    }
}