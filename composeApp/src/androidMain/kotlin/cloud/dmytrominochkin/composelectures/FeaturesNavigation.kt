package cloud.dmytrominochkin.composelectures

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.DrawerState
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable

fun NavGraphBuilder.featureScreen(
    name: String,
    route: String,
    drawerState: DrawerState,
    content: @Composable () -> Unit
) {
    composable(route) {
        Scaffold(
            topBar = {
                DrawerAppBar(
                    drawerState = drawerState,
                    title = name
                )
            }
        ) { paddingValues ->
            Surface(modifier = Modifier.padding(paddingValues)) {
                content()
            }
        }
    }
}
