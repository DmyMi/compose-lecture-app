package cloud.dmytrominochkin.composelectures

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.DrawerState
import androidx.compose.material3.DrawerValue
import androidx.compose.material3.Icon
import androidx.compose.material3.ModalDrawerSheet
import androidx.compose.material3.ModalNavigationDrawer
import androidx.compose.material3.NavigationDrawerItem
import androidx.compose.material3.Text
import androidx.compose.material3.rememberDrawerState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import cloud.dmytrominochkin.composelectures.animation.AnimationScreen
import cloud.dmytrominochkin.composelectures.architecture.ArchitectureScreen
import cloud.dmytrominochkin.composelectures.basics.BasicsScreen
import cloud.dmytrominochkin.composelectures.injection.InjectionScreen
import cloud.dmytrominochkin.composelectures.internet.InternetScreen
import cloud.dmytrominochkin.composelectures.internet.di.internetModules
import cloud.dmytrominochkin.composelectures.navigation.NavigationScreen
import cloud.dmytrominochkin.composelectures.resources.ResourcesScreen
import cloud.dmytrominochkin.composelectures.room.RoomScreen
import cloud.dmytrominochkin.composelectures.room.di.roomModules
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.under_construction
import cloud.dmytrominochkin.composelectures.state.StateScreen
import cloud.dmytrominochkin.composelectures.ui.components.UnderConstruction
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.koin.android.ext.koin.androidContext
import org.koin.compose.KoinIsolatedContext
import org.koin.dsl.koinApplication

@Composable
fun MainScreenAndroid(
    coroutineScope: CoroutineScope = rememberCoroutineScope(),
    drawerState: DrawerState = rememberDrawerState(initialValue = DrawerValue.Closed),
    navController: NavHostController = rememberNavController()
) {
    ModalNavigationDrawer(
        drawerState = drawerState,
        drawerContent = {
            ModalDrawerSheet {
                MenuContent(menus, onMenuClick = { menuRoute ->
                    coroutineScope.launch {
                        drawerState.close()
                    }
                    navController.navigate(menuRoute)
                }) { idx, menu, (value, setValue), onMenuClick ->
                    NavigationDrawerItem(
                        label = { Text(text = menu.title) },
                        icon = { Icon(imageVector = menu.icon, contentDescription = null) },
                        selected = value == idx,
                        onClick = {
                            setValue(idx)
                            onMenuClick(menu.route)
                        }
                    )
                }
            }
        }) {
        NavHost(navController = navController, startDestination = aboutRoute) {
            featureScreen(
                name = "About",
                route = aboutRoute,
                drawerState = drawerState
            ) {
                AboutScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }
            featureScreen(
                name = "Basics",
                route = basicsRoute,
                drawerState = drawerState
            ) {
                BasicsScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "Resources",
                route = resourcesRoute,
                drawerState = drawerState
            ) {
                ResourcesScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "State",
                route = stateRoute,
                drawerState = drawerState
            ) {
                StateScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "Animation",
                route = animationRoute,
                drawerState = drawerState
            ) {
                AnimationScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "Navigation",
                route = navigationRoute,
                drawerState = drawerState
            ) {
                NavigationScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "Architecture",
                route = architectureRoute,
                drawerState = drawerState
            ) {
                ArchitectureScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "Injection",
                route = injectionRoute,
                drawerState = drawerState
            ) {
                InjectionScreen(
                    modifier = Modifier
                        .fillMaxSize()
                )
            }

            featureScreen(
                name = "Internet",
                route = internetRoute,
                drawerState = drawerState
            ) {
                val context = LocalContext.current
                KoinIsolatedContext(context = koinApplication {
                    androidContext(context.applicationContext)
                    modules(internetModules())
                }) {
                    InternetScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }

            featureScreen(
                name = "Room",
                route = roomRoute,
                drawerState = drawerState
            ) {
                val context = LocalContext.current
                KoinIsolatedContext(context = koinApplication {
                    androidContext(context.applicationContext)
                    modules(roomModules())
                }) {
                    RoomScreen(
                        modifier = Modifier
                            .fillMaxSize()
                    )
                }
            }
            featureScreen(
                name = "Extra",
                route = extraRoute,
                drawerState = drawerState
            ) {
                UnderConstruction(
                    label = Res.string.under_construction,
                    path = "files/underconstruction.json",
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    }
}