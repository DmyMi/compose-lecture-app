package cloud.dmytrominochkin.composelectures

import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import cloud.dmytrominochkin.composelectures.shared.TranslationPreview

@TranslationPreview
@Composable
fun AboutPreview() {
    MaterialTheme {
        Surface {
            AboutScreen()
        }
    }
}