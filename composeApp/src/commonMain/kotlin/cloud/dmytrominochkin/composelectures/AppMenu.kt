package cloud.dmytrominochkin.composelectures

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Animation
import androidx.compose.material.icons.filled.Architecture
import androidx.compose.material.icons.filled.FolderOpen
import androidx.compose.material.icons.filled.ForkRight
import androidx.compose.material.icons.filled.Foundation
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Memory
import androidx.compose.material.icons.filled.QuestionMark
import androidx.compose.material.icons.filled.Storage
import androidx.compose.material.icons.filled.Vaccines
import androidx.compose.material.icons.filled.Wifi
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.mutableIntStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.vector.ImageVector

const val aboutRoute = "about"
const val basicsRoute = "basics"
const val resourcesRoute = "resources"
const val stateRoute = "state"
const val animationRoute = "animation"
const val navigationRoute = "navigation"
const val architectureRoute = "architecture"
const val injectionRoute = "injection"
const val internetRoute = "internet"
const val roomRoute = "room"
const val extraRoute = "extra"

data class AppMenu(val title: String, val icon: ImageVector, val route: String)

val menus = arrayOf(
    AppMenu("About", Icons.Filled.Home, aboutRoute),
    AppMenu("Basics", Icons.Filled.Foundation, basicsRoute),
    AppMenu("Resources", Icons.Filled.FolderOpen, resourcesRoute),
    AppMenu("State", Icons.Filled.Memory, stateRoute),
    AppMenu("Animation", Icons.Filled.Animation, animationRoute),
    AppMenu("Navigation", Icons.Filled.ForkRight, navigationRoute),
    AppMenu("Architecture", Icons.Filled.Architecture, architectureRoute),
    AppMenu("Injection", Icons.Filled.Vaccines, injectionRoute),
    AppMenu("Internet", Icons.Filled.Wifi, internetRoute),
    AppMenu("Room", Icons.Filled.Storage, roomRoute),
    AppMenu("Extra", Icons.Filled.QuestionMark, extraRoute),
)

@Composable
fun MenuContent(
    menus: Array<AppMenu>,
    onMenuClick: (String) -> Unit,
    content: @Composable (idx: Int, menu: AppMenu, selected: MutableIntState, onMenuClick: (String) -> Unit) -> Unit
) {
    val selected = remember { mutableIntStateOf(0) }
    menus.forEachIndexed { idx, menu ->
        content(idx, menu, selected, onMenuClick)
    }
}