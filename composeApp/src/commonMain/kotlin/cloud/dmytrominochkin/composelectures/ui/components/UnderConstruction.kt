package cloud.dmytrominochkin.composelectures.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import kottieAnimation.KottieAnimation
import kottieComposition.KottieCompositionSpec
import kottieComposition.animateKottieCompositionAsState
import kottieComposition.rememberKottieComposition
import org.jetbrains.compose.resources.ExperimentalResourceApi
import org.jetbrains.compose.resources.StringResource
import org.jetbrains.compose.resources.stringResource
import utils.KottieConstants

@OptIn(ExperimentalResourceApi::class)
@Composable
fun UnderConstruction(
    label: StringResource,
    path: String,
    modifier: Modifier = Modifier
) {
    var animation by remember { mutableStateOf("") }

    LaunchedEffect(Unit){
        animation = Res.readBytes(path).decodeToString()
    }

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .wrapContentSize(align = Alignment.Center)
            .padding(32.dp)
    ) {
        Text(
            text = stringResource(resource = label),
            style = MaterialTheme.typography.titleLarge,
        )

        val composition = rememberKottieComposition(
            spec = KottieCompositionSpec.File(animation)
        )

        val animationState by animateKottieCompositionAsState(
            composition = composition,
            iterations = KottieConstants.IterateForever
        )

        KottieAnimation(
            composition = composition,
            progress = { animationState.progress },
            modifier = Modifier.requiredSize(200.dp)
        )
    }
}