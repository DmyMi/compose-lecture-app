package cloud.dmytrominochkin.composelectures.shared

import androidx.compose.desktop.ui.tooling.preview.Preview

/**
 * Actual implementation of [TranslationPreview] for the Desktop platform.
 *
 * Uses a type alias to map [TranslationPreview] to the [Preview] annotation.
 *
 * Type Alias:
 * - A feature in Kotlin that allows you to provide an alternative name for an existing type.
 *
 * [Preview]:
 * - Similar to the Android `Preview` annotation but used for Jetpack Compose Desktop.
 * - Enables the rendering of composable functions in the IDE's preview pane for desktop applications.
 */
actual typealias TranslationPreview = Preview