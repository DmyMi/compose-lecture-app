package cloud.dmytrominochkin.composelectures.shared.components.feed

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import java.awt.Desktop

@Composable
actual fun SharedShareButton() {
    IconButton(onClick = {
        val desktop = Desktop.getDesktop()
        if (Desktop.isDesktopSupported() && desktop.isSupported(Desktop.Action.MAIL)) {
            desktop.mail()
        } else {
            println("No email client")
        }
    }) {
        Icon(Icons.Default.Share, contentDescription = "Share")
    }
}