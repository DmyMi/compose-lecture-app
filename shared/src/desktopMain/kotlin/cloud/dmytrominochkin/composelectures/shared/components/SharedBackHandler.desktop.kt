package cloud.dmytrominochkin.composelectures.shared.components

import androidx.compose.runtime.Composable

@Composable
actual fun SharedBackHandler(isEnabled: Boolean, onBack: () -> Unit) {
    // Do nothing, as Desktop has no concept of back event
}