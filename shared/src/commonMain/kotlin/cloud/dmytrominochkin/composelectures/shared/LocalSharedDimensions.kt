package cloud.dmytrominochkin.composelectures.shared

import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

/**
 * Holds the standard padding dimensions for the application.
 *
 * This data class encapsulates commonly used padding values in [Dp] units,
 * providing a centralized place to define and access padding throughout the app.
 *
 * [Dp]:
 * - Stands for Density-independent Pixels.
 * - A unit of measurement that allows UI elements to appear the same size across different screen densities.
 * - Ensures consistent sizing across various screen resolutions and densities.
 *
 * @property paddingSmall The small padding value, defaulting to 8.dp.
 * @property paddingMedium The medium padding value, defaulting to 16.dp.
 * @property paddingLarge The large padding value, defaulting to 24.dp.
 */
internal data class SharedAppDimensions(
    val paddingSmall: Dp = 8.dp,
    val paddingMedium: Dp = 16.dp,
    val paddingLarge: Dp = 24.dp
)

/**
 * A composition local that provides the current [SharedAppDimensions].
 *
 * [androidx.compose.runtime.CompositionLocal]:
 * - A way to propagate values down the compose tree without passing them explicitly through function parameters.
 * - Useful for providing theming, configurations, or other data that many composables need to access.
 *
 * [staticCompositionLocalOf]:
 * - A function that creates a static composition local.
 * - Unlike `compositionLocalOf`, the value provided cannot change during recomposition.
 *
 * In this case, the default [SharedAppDimensions] are sufficient for the app's needs,
 * so overriding it using a `CompositionLocalProvider` is not required.
 *
 * @see SharedAppDimensions
 */
internal val LocalSharedDimensions = staticCompositionLocalOf {
    SharedAppDimensions()
}