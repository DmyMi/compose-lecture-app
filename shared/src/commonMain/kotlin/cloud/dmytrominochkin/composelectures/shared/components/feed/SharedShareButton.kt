package cloud.dmytrominochkin.composelectures.shared.components.feed

import androidx.compose.runtime.Composable

@Composable
expect fun SharedShareButton()