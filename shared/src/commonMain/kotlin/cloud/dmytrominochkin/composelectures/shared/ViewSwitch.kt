package cloud.dmytrominochkin.composelectures.shared

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Switch
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier

/**
 * Displays a switch with labels on both sides, allowing the user to toggle between two options.
 *
 * This is a [Composable] function in Jetpack Compose that creates a [Row] containing a [Switch]
 * widget flanked by labels on the left and right.
 *
 * [Switch]:
 * - A composable that represents a Material Design switch.
 * - Allows users to toggle between two states: on and off.
 * - Can be customized with colors, sizes, and other properties.
 * - Typically used in settings or preference screens to enable or disable options.
 *
 * [LocalSharedDimensions]:
 * - A composition local that provides shared dimension values, such as padding and spacing.
 * - Allows for consistent spacing and sizing throughout the app without passing them explicitly.
 * - Accessed using `LocalSharedDimensions.current`, which provides the current [SharedAppDimensions].
 *
 * @param isChecked The current checked state of the switch.
 * @param setCheck A lambda function to update the checked state.
 * @param leftLabel The label displayed to the left of the switch.
 * @param rightLabel The label displayed to the right of the switch.
 */
@Composable
fun ViewSwitch(
    switchState: ViewSwitchState,
    leftLabel: String,
    rightLabel: String) {

    val paddingSmall = LocalSharedDimensions.current.paddingSmall

    Row(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Text(text = leftLabel, modifier = Modifier.padding(end = paddingSmall))
        Switch(
            checked = switchState.isChecked,
            onCheckedChange = switchState::setCheck
        )
        Text(text = rightLabel, modifier = Modifier.padding(start = paddingSmall))
    }
}

/**
 * A basic state holder class that manages UI state and logic.
 *
 * This class encapsulates the UI state and the logic that modifies it.
 * The state is exposed using `var` properties with `mutableStateOf`, allowing Compose to observe changes.
 *
 * @property isChecked The current switch value.
 */
class ViewSwitchState {
    var isChecked: Boolean by mutableStateOf(false)
        private set

    fun setCheck(checked: Boolean) {
        isChecked = checked
    }
}

/**
 * Remembers and returns an instance of [ViewSwitchState].
 *
 * This custom `remember` function ensures that the same instance of [ViewSwitchState] is used across recompositions.
 *
 * @return An instance of [ViewSwitchState].
 */
@Composable
fun rememberViewSwitchState(): ViewSwitchState {
    return remember { ViewSwitchState() }
}