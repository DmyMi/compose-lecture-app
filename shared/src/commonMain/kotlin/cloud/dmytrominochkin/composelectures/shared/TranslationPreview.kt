package cloud.dmytrominochkin.composelectures.shared

/**
 * An expected annotation used for generating previews with different translations (locales).
 *
 * This annotation is expected to be implemented in platform-specific code.
 */
expect annotation class TranslationPreview