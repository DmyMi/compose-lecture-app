package cloud.dmytrominochkin.composelectures.shared.components.feed

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions

@Composable
fun SharedFeedItem(
    userName: String,
    lastOnline: String,
    onClick: () -> Unit,
    avatar: @Composable () -> Unit,
    cover: @Composable () -> Unit,
    modifier: Modifier = Modifier
) {
//    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalSharedDimensions.current.paddingMedium
    Column(
        modifier = modifier
            .clickable(onClick = onClick)
            .padding(
                top = mediumPadding / 2,
                bottom = mediumPadding / 2,
                start = mediumPadding,
                end = mediumPadding
            )
            .fillMaxWidth()
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically
        ) {
            avatar()
            Spacer(modifier = Modifier.padding(mediumPadding))
            Column {
                Text(
                    text = userName,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Medium)
                )
                Text(
                    text = lastOnline,
                    style = MaterialTheme.typography.bodyLarge.copy(fontWeight = FontWeight.Light)
                )
            }
        }
        Spacer(modifier = Modifier.padding(mediumPadding))
        cover()
    }
}