package cloud.dmytrominochkin.composelectures.shared.components.dialog

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AccountCircle
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions
import cloud.dmytrominochkin.composelectures.shared.resources.*
import org.jetbrains.compose.resources.stringResource


@Composable
fun SharedUsernameDialog(
    name: String,
    onNameChange: (String) -> Unit,
    onDismiss: () -> Unit,
    onConfirm: () -> Unit
) {
    val dimensions = LocalSharedDimensions.current
//    val padding = dimensionResource(id = R.dimen.padding_medium)
    val padding = dimensions.paddingMedium
//    val smallPadding = dimensionResource(id = R.dimen.padding_small)
    val smallPadding = dimensions.paddingSmall
    Dialog(onDismissRequest = onDismiss) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .height(375.dp)
                .padding(padding),
            shape = RoundedCornerShape(padding),
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Icon(
                    Icons.Default.AccountCircle,
                    contentDescription = null,
                    modifier = Modifier.size(100.dp)
                )
                TextField(
                    value = name,
                    onValueChange = onNameChange,
                    modifier = Modifier.padding(padding),
                    placeholder = { Text(stringResource(Res.string.your_name))}
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceEvenly,
                ) {
                    TextButton(
                        onClick = onDismiss,
                        modifier = Modifier.padding(smallPadding),
                    ) {
                        Text(stringResource(Res.string.cancel), style = MaterialTheme.typography.titleLarge)
                    }
                    TextButton(
                        onClick = onConfirm,
                        modifier = Modifier.padding(smallPadding),
                    ) {
                        Text(stringResource(Res.string.save), style = MaterialTheme.typography.titleLarge)
                    }
                }
                Text(
                    text = stringResource(Res.string.not_a_real),
                    style = MaterialTheme.typography.labelSmall,
                    fontWeight = FontWeight.Light)
            }
        }
    }
}