package cloud.dmytrominochkin.composelectures.shared.components

import androidx.compose.runtime.Composable

@Composable
expect fun SharedBackHandler(isEnabled: Boolean = true, onBack: () -> Unit)