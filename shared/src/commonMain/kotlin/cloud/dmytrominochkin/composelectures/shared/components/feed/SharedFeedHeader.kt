package cloud.dmytrominochkin.composelectures.shared.components.feed

import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.feed
import cloud.dmytrominochkin.composelectures.shared.resources.friend
import cloud.dmytrominochkin.composelectures.shared.resources.hello
import org.jetbrains.compose.resources.stringResource


@Composable
fun SharedFeedHeader(
    onIconClick: () -> Unit,
    userName: String,
    modifier: Modifier = Modifier
) {
    val dimensions = LocalSharedDimensions.current
    val mediumPadding = dimensions.paddingMedium
    val largePadding = dimensions.paddingLarge
    Surface(
        color = MaterialTheme.colorScheme.primaryContainer,
        contentColor = MaterialTheme.colorScheme.onSurface,
        modifier = modifier.fillMaxWidth()
    ) {
        Column {
            Spacer(modifier = Modifier.size(largePadding))
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = modifier.fillMaxWidth()
            ) {
                Column {
                    Text(
                        text = stringResource(resource = Res.string.hello),
                        style = MaterialTheme.typography.bodyLarge,
                        modifier = Modifier.padding(start = mediumPadding)
                    )
                    Text(
                        text = userName.ifBlank { stringResource(Res.string.friend) },
                        style = MaterialTheme.typography.bodyMedium,
                        modifier = Modifier.padding(start = mediumPadding)
                    )
                }
                Row(horizontalArrangement = Arrangement.End) {
                    IconButton(onClick = onIconClick) {
                        Icon(Icons.Default.Refresh, contentDescription = null)
                    }
                    SharedShareButton()
                }
            }
            Spacer(modifier = Modifier.size(mediumPadding))
            SharedRoundedHeader(title = stringResource(resource = Res.string.feed))
        }
    }
}