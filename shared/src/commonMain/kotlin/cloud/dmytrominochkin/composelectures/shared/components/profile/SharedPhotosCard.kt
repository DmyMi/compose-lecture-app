package cloud.dmytrominochkin.composelectures.shared.components.profile

import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.togetherWith
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions
import cloud.dmytrominochkin.composelectures.shared.ViewSwitch
import cloud.dmytrominochkin.composelectures.shared.rememberViewSwitchState
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.photos
import org.jetbrains.compose.resources.stringResource

@Composable
fun <T> SharedPhotosCard(
    groupedImages: Map<String, List<T>>,
    modifier: Modifier = Modifier,
    gridContent: @Composable (image: T) -> Unit
) {
    val groups = groupedImages.keys.toList()
    //    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalSharedDimensions.current.paddingMedium
    val checkedState = rememberViewSwitchState()
    Surface(
        modifier = modifier.fillMaxWidth(),
        shadowElevation = 2.dp,
        shape = RoundedCornerShape(10, 10, 0, 0)
    ) {
        Column {
            var selectedGroup by rememberSaveable {
                mutableStateOf(groups.first())
            }
            Text(
                text = stringResource(Res.string.photos),
                style = MaterialTheme.typography.headlineSmall,
                modifier = Modifier.padding(
                    start = mediumPadding,
                    end = mediumPadding,
                    top = mediumPadding
                )
            )
            ViewSwitch(checkedState, "Lazy", "Custom")
            SharedPhotosTab(
                groups = groups,
                selectedGroup = selectedGroup,
                onSelected = { grp -> selectedGroup = grp }
            )
            AnimatedContent(
                targetState = checkedState.isChecked,
                label = "grid",
                transitionSpec = {
                    fadeIn(tween(1000)) togetherWith fadeOut(tween(500))
                }
            ) {
                if (it) {
                    SharedDynamicPhotosGrid(
                        images = groupedImages.getValue(selectedGroup),
                        modifier = Modifier
                            .padding(
                                top = mediumPadding / 2,
                                start = mediumPadding,
                                end = mediumPadding,
                                bottom = mediumPadding
                            ),
                        content = gridContent
                    )
                } else {
                    SharedPhotosGrid(
                        images = groupedImages.getValue(selectedGroup),
                        modifier = Modifier
                            .padding(
                                top = mediumPadding / 2,
                                start = mediumPadding,
                                end = mediumPadding,
                                bottom = mediumPadding
                            ),
                        content = { image -> gridContent(image) }
                    )
                }
            }
        }
    }
}