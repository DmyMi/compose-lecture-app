package cloud.dmytrominochkin.composelectures.shared.components.profile

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyGridItemScope
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions

@Composable
fun <T> SharedPhotosGrid(
    images: List<T>,
    modifier: Modifier = Modifier,
    content: @Composable LazyGridItemScope.(image: T) -> Unit
) {
//    val smallPadding = dimensionResource(id = R.dimen.padding_small)
    val smallPadding = LocalSharedDimensions.current.paddingSmall
    LazyVerticalGrid(
        columns = GridCells.Fixed(3),
        verticalArrangement = Arrangement.spacedBy(smallPadding),
        horizontalArrangement = Arrangement.spacedBy(smallPadding),
        modifier = modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    ) {
        items(images, itemContent = content)
    }
}