package cloud.dmytrominochkin.composelectures.shared.components.profile

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun SharedTags(tags: List<String>, modifier: Modifier = Modifier) {
//    val padding = dimensionResource(id = R.dimen.padding_small)
    val padding = LocalSharedDimensions.current.paddingSmall
    FlowRow(
        modifier = modifier
            .padding(PaddingValues(start = padding * 2, end = padding)),
        horizontalArrangement = Arrangement.spacedBy(padding)
    ) {
        tags.forEach { tag ->
            Text(
                text = tag,
                style = MaterialTheme.typography.titleMedium,
                modifier = Modifier
                    .border(
                        1.dp,
                        MaterialTheme.colorScheme.onSurface.copy(alpha = 0.38f),
                        CircleShape
                    )
                    .padding(padding)
            )
        }
    }
}