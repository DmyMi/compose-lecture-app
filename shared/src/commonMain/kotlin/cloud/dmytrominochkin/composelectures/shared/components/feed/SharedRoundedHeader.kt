package cloud.dmytrominochkin.composelectures.shared.components.feed

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions

@Composable
fun SharedRoundedHeader(
    title: String,
    modifier: Modifier = Modifier
) {
    Surface(
        modifier = modifier
            .fillMaxWidth()
            .height(50.dp),
        shadowElevation = 1.dp,
        shape = RoundedCornerShape(50, 50, 0, 0)
    ) {
//        val padding = dimensionResource(id = R.dimen.padding_medium)
        val padding = LocalSharedDimensions.current.paddingMedium
        Text(
            text = title,
            modifier = Modifier.padding(start = padding, top = padding, end = padding),
            style = MaterialTheme.typography.headlineSmall
        )
    }
}