package cloud.dmytrominochkin.composelectures.shared.components.profile


import androidx.compose.foundation.layout.*
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import cloud.dmytrominochkin.composelectures.shared.LocalSharedDimensions
import cloud.dmytrominochkin.composelectures.shared.resources.Res
import cloud.dmytrominochkin.composelectures.shared.resources.followers
import cloud.dmytrominochkin.composelectures.shared.resources.following
import org.jetbrains.compose.resources.stringResource


@Composable
fun SharedProfileHeader(
    userName: String,
    numOfFollowers: Int,
    numOfFollowing: Int,
    onFollow: () -> Unit = {},
    modifier: Modifier = Modifier,
    avatar: @Composable () -> Unit
) {
    //    val mediumPadding = dimensionResource(id = R.dimen.padding_medium)
    val mediumPadding = LocalSharedDimensions.current.paddingMedium
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .padding(start = mediumPadding, end = mediumPadding)
    ) {
        avatar()
        Spacer(modifier = Modifier.size(mediumPadding))
        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(text = userName, style = MaterialTheme.typography.titleLarge)
            Row (
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.fillMaxWidth()
            ) {
                SharedFollowersInfo(number = numOfFollowers, text = stringResource(Res.string.followers))
                SharedFollowersInfo(number = numOfFollowing, text = stringResource(Res.string.following))
                SharedAnimatedFollowButton(onFollow = onFollow)
            }
        }
    }
}