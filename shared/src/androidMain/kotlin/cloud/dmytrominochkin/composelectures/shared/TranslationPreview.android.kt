package cloud.dmytrominochkin.composelectures.shared

import androidx.compose.ui.tooling.preview.Preview

/**
 * Actual implementation of [TranslationPreview] for the Android platform.
 *
 * Applies multiple [Preview] annotations to generate UI previews for different locales.
 *
 * In this implementation, [TranslationPreview] includes:
 * - A preview for the English locale.
 * - A preview for the Ukrainian locale.
 */
@Preview(
    name = "English",
    group = "locale",
    locale = "en"
)
@Preview(
    name = "Ukrainian",
    group = "locale",
    locale = "uk"
)
actual annotation class TranslationPreview