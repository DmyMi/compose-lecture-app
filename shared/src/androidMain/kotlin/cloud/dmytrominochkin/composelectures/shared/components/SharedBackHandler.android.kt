package cloud.dmytrominochkin.composelectures.shared.components

import androidx.compose.runtime.Composable
import androidx.activity.compose.BackHandler as AndroidBackHandler

@Composable
actual fun SharedBackHandler(isEnabled: Boolean, onBack: () -> Unit) {
    AndroidBackHandler(isEnabled, onBack)
}