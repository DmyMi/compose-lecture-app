#!/bin/zsh
#---------------------------------------------------------------
# Given an xxhdpi image or an composeApp Icon (launcher), this script
# creates different dpis resources
#
# Place this script, as well as the source image, inside composeApp
# folder and execute it passing the image filename as argument
#
# Example:
# ./scale_image_for_resolution.sh ic_launcher.png
# OR
# ./scale_image_for_resolution.sh my_cool_xxhdpi_image.png
#---------------------------------------------------------------

echo " Creating different dimensions (dips) of "$1" ..."

if [ $1 = "ic_launcher.png" ]; then
    echo "  App icon detected"

    magick ic_launcher.png -resize 192x192 composeApp/src/androidMain/res/mipmap-xxxhdpi/ic_launcher.webp
    magick ic_launcher.png -resize 144x144 composeApp/src/androidMain/res/mipmap-xxhdpi/ic_launcher.webp
    magick ic_launcher.png -resize 96x96 composeApp/src/androidMain/res/mipmap-xhdpi/ic_launcher.webp
    magick ic_launcher.png -resize 72x72 composeApp/src/androidMain/res/mipmap-hdpi/ic_launcher.webp
    magick ic_launcher.png -resize 48x48 composeApp/src/androidMain/res/mipmap-mdpi/ic_launcher.webp
    rm -i ic_launcher.png
else

    magick $1 -resize 67% composeApp/src/androidMain/res/drawable-xhdpi/$1
    magick $1 -resize 50% composeApp/src/androidMain/res/drawable-hdpi/$1
    magick $1 -resize 33% composeApp/src/androidMain/res/drawable-mdpi/$1
    mv $1 composeApp/src/androidMain/res/drawable-xxhdpi/$1

fi

echo " Done"