# Jetpack Compose Lectures App

## Description

Compose Lectures is a Compose Multiplatform app designed to show the basic and advanced features of Jetpack Compose.

It is used as a companion application for Mobile Programming course.

### Android

Screenshots:

![](images/mobile_01.png)
![](images/mobile_02.png)
![](images/mobile_03.png)

### Desktop

To run the Desktop app:

```bash
./gradlew composeApp:desktopRun -DmainClass=cloud.dmytrominochkin.composelectures.DesktopAppKt
```

Screenshots:

![](images/desktop_01.png)
![](images/desktop_02.png)
![](images/desktop_03.png)

## Lectures

### Jetpack Compose introduction

In [Basics lecture](lectures/basics) we created a UI structure of [ProfileHeader](lectures/basics/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/basics/ui/profile/ProfileHeader.kt) and [FollowersInfo](lectures/basics/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/basics/ui/profile/FollowersInfo.kt) using basic composable components like `Text`, `Image`, `Row`, `Column` and simple modifiers.

Also, `expect / actual` for multiplatform development was introduced using the [getPlatform](lectures/basics/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/basics/Platform.kt) function.

### Resources & Themes

In [Resources lecture](lectures/resources) we added style to our UI with a custom color scheme and through the use of typography.
Additionally, we used resources for dimensions and translation of static text in existing and new composables.

* Added [FeedItem](lectures/resources/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/resources/ui/feed/FeedItem.kt)
* Added [Profile](lectures/resources/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/resources/ui/profile/Profile.kt)
* Added [PhotosCard](lectures/resources/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/resources/ui/profile/PhotosCard.kt) component placeholder.
* Modified `ProfileHeader` to used resources.

The resources are going to be re-used for all the lectures, so the [Theme](shared/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/shared/theme)
and other [multiplatform resources](shared/src/commonMain/composeResources) were created in the [shared](shared) subproject.

> [!NOTE]  
> Check the [shared](shared) subproject README for list of all extracted components that are re-used in following lectures.

Compose Multiplatform does not support dimension resources, so a [CompositionLocal](lectures/resources/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/resources/LocalDimensions.kt) approach was introduced.

For Android, a custom [TranslationPreview](shared/src/androidMain/kotlin/cloud/dmytrominochkin/composelectures/shared/TranslationPreview.android.kt) annotation was created to see translated previews of our designs.

### Scrollable layouts & state

In [State lecture](lectures/state) we added 3 types of scrollable layouts to the application, click handling and basic state management.

* Added List layout to [Feed](lectures/state/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/state/ui/feed/Feed.kt)
* Added Grid layout to `Profile` component [PhotosGrid](lectures/state/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/state/ui/profile/PhotosGrid.kt)
* Added Flow layout to `Profile` component [Tags](lectures/state/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/state/ui/profile/Tags.kt)
* Added state to `Feed`
* Added state hoisting to `Feed` & [UsernameDialog](lectures/state/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/state/ui/dialog/UsernameDialog.kt)
* Introduced plain class state holder approach in [ViewSwitch](shared/src/commonMain/kotlin/cloud/dmytrominochkin/composelectures/shared/ViewSwitch.kt)

<!--
### Animation
In [merge request #4](https://gitlab.com/DmyMi/compose-lecture-app/-/merge_requests/4) we added different types of animation and created a simple screen transition.
* Added [MainScreen](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/MainScreen.kt)
composable with [Scaffold](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/MainScreen.kt#L36),
[TopAppBar](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/MainScreen.kt#L67)
and a [Crossfade](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/MainScreen.kt#L43) transition between `Feed` and `Profile`.
* Used `MainScreen` as application screen in [MainActivity](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/MainActivity.kt)
* Added an [AnimatedFollowButton](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/profile/AnimatedFollowButton.kt) using `AnimatedContent`.
* Added [PhotosTab](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/profile/PhotosTab.kt) with selectable tabs
and Color animation using [animateColorAsState](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/profile/PhotosTab.kt#L47)
and also selection animation using [updateTransition](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/profile/PhotosTab.kt#L69)
* `AnimatedVisibility` was shown in previous merge request

> Note: With this merge request the `compileSdk` and `targetSdk` are both set to `34`. Check changes in [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/30e14b1d13a9aeaf57b8937f9acdc71181b0990d/app/build.gradle.kts)

### Navigation
In [merge request #5](https://gitlab.com/DmyMi/compose-lecture-app/-/merge_requests/5) we added Jetpack Navigation component with some basic navigation features.
* Added a dependency in [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/c9267aa34cc40a318bec4a0666df2c2d2bbbc8c5/app/build.gradle.kts#L62)
* Added NavController, NavHost and basic navgraph in [MainScreenNav](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/0feadd46a392ad99eda90723c9be31d073f03e0f/app/src/main/java/cloud/dmytrominochkin/composelecture/MainScreenNav.kt#L40)
* Added DeepLinks Intent filter in [AndroidManifest.xml](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/c9267aa34cc40a318bec4a0666df2c2d2bbbc8c5/app/src/main/AndroidManifest.xml#L24)
* Implemented a share button using Intents in [FeedHeader](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/0feadd46a392ad99eda90723c9be31d073f03e0f/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/feed/FeedHeader.kt#L55)

### App architecture
In [merge request #6](https://gitlab.com/DmyMi/compose-lecture-app/-/merge_requests/6) we added Jetpack ViewModel component and implemented basic layered architecture.
* Added a dependency in [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/build.gradle.kts#L63)
* Added a [MockNetDataSource](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/src/main/java/cloud/dmytrominochkin/composelecture/data/datasource/MockNetDataSource.kt) Data Source to simulate networking requests
* Added a [MockUserRepository](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/src/main/java/cloud/dmytrominochkin/composelecture/data/repository/MockUserRepository.kt) to expose app data to the rest of the application
* Added a [FeedViewModel](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/feed/FeedViewModel.kt) as a state holder of the UI state of the Feed screen and to transform app data to UI state. Consumed the UI state in [Feed](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/feed/Feed.kt#L36)
* Added a second [ProfileViewModel](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/profile/ProfileViewModel.kt) for the Profile screen for demonstration of different lifecycle of ViewModels bound to the `NavGraph`'s `BackStackEntry`s. Consumed the UI state in [Profile](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/ae4472ca74c481575cfcda8969def6f2335f79a6/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/profile/Profile.kt#L36)
* Added a Snackbar and loading indicators yo both Feed & Profile

### Room Database
In [merge request #7](https://gitlab.com/DmyMi/compose-lecture-app/-/merge_requests/7) we added Room to persist structured data in a database.
* Added a dependency in [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/build.gradle.kts#L65-69)

> Note: Also a Kotlin Annotation Processing plugin was added. Check changes in [build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/build.gradle.kts) and
> [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/build.gradle.kts)

* Added a [DrawableDictionary](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/util/DrawableDictionary.kt) helper class to map Int values that can be stored in database to Android resources so we can reuse our previous data model.
* Added [Entities](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/data/db/Enitities.kt) to represent our previous data model as structured data. Schema looks as the following diagram:

![](images/db.png)

* Added a [UserDao](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/data/db/UserDao.kt) as a Data Source for database data.
* Added a [SocialDatabase](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/data/db/SocialDatabase.kt) Room database class to initialize the database connection and create all the necessary objects.
* Added a [DbUserRepository](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/data/repository/DbUserRepository.kt) to expose app data to the rest of the application.
* As it's not a good practice to reference Android system objects (like `Context`) in ViewModels and Composables, a home made dependency injection was implemented:
  * Added a [AppContainer](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/AppContainer.kt) to serve as a container for home-made dependency injection and initialize our repository.
  * Added a [SocialApplication](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/SocialApplication.kt) `Application` class to be both a singleton store for or container and guarantee 
    initialization of container before any other component, as `Application` is one of the first object to be created when app starts.
  * Modified [AndroidManifest.xml](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/AndroidManifest.xml#L6) to reference the name of our custom `Application` so the system known which class to create.
  * Added a [AppViewModelProvider](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/AppViewModelProvider.kt) to create instance of ViewModel for the entire app.
  * Modified Composables that require ViewModel to use our factory, for example [Feed](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/feed/Feed.kt#L35)

> Note: With this merge request the Android Gradle Plugin version `AGP` is updated to `8.2.0`. Also Java version is set to `17`. Check changes in [build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/build.gradle.kts) and 
> [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/1fdf75007463b6d0259f32ecaba7311ca544c0dd/app/build.gradle.kts)

### Internet
In [merge request #8](https://gitlab.com/DmyMi/compose-lecture-app/-/merge_requests/8) we added Ktor Client and Coil to work with remote servers to load data and images. Also an example of Network bound resource was implemented.
* Added a dependency in [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/build.gradle.kts#L69-72)

> Note: Also a Kotlin Serialization plugin was added. Check changes in [build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/build.gradle.kts#L6) and
> [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/build.gradle.kts#L5)

* Added [ApiModels](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/data/api/ApiModels.kt) to represent remote server's data.
* Added a [NetworkDataSource](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/data/api/NetworkDataSource.kt) to encapsulate remote source of data.
* Added extension functions to serve as [data mappers](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/data/repository/Mappers.kt) between db, application and api models.
* Added a [DataState](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/data/DataState.kt) to represent different states of working with data in application.
* Implemented an example of [NetworkBoundResource](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/data/repository/UserRepositoryImpl.kt) algorithm in the repository.
* Modified ViewModels to use our new approach, for example [FeedViewModel](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/feed/FeedViewModel.kt#L22)
* Modified Composable functions to use `AsyncImage` instead of simple image to load images from Internet, for example [FeedItem](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/ui/feed/FeedItem.kt)
* Modified [AndroidManifest.xml](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/AndroidManifest.xml#L5) to include`INTERNET` permission.
* Modified [AppContainer](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/src/main/java/cloud/dmytrominochkin/composelecture/AppContainer.kt) to create both Ktor Client and new Network Data Source.

> Note: With this merge request Kotlin and Compose were update to latest stable versions. Also KAPT was replaced with KSP for `Room`. Check changes in [build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/build.gradle.kts#L4-5) and
> [app/build.gradle.kts](https://gitlab.com/DmyMi/compose-lecture-app/-/blob/9b4d7fe95441ada04c85f61a13f1c3c63401e975/app/build.gradle.kts)
-->

## License

```
Copyright 2024 Dmytro Minochkin

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```